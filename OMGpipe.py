#!/usr/bin/env python3
"""
Created on Fri Apr  6 13:08:36 2018

@author: Bob Buckley, ANU Bioinformatics Consultancy, John Curtin SChool of Medical Research, ANU

pre-config for the dorun.py module
"""

import os
import sys
import glob
#import argparse
import configparser

import dorun

version = "0.1"

def main():

    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    ext = 'fastq.gz'
    mydir = sys.argv[-1] if sys.argv[-1].startswith(os.path.sep) else \
                os.getcwd() if sys.argv[-1]=='.' else \
                os.path.join(os.getcwd(), sys.argv[-1]) 
    if sys.argv[-1]!='.':
        # the following will fail if the directory does not exist
        os.chdir(sys.argv[-1])
        sys.argv[-1] = '.'
    dirname = os.path.split(mydir)[1]
    x = dirname.split('-', 1)
    sampno = x[0]
    datasetID = x[1] if len(x)>1 else ''
    # files are in defined places - raw/*, trim/* ...
    def fnjoin(a, b, x):
        c = '.'.join((b, x))
        return c if a=="." else os.path.join(a, c)
    r1files = glob.glob(fnjoin(sys.argv[-1], 'raw/*_L0*_R1*', ext))
    r2files = glob.glob(fnjoin(sys.argv[-1], 'raw/*_L0*_R2*', ext))
    ddfiles = sorted(glob.glob(fnjoin(sys.argv[-1], 'trim/*_DD_R[12]', ext)))

    zs = tuple(os.path.split(x)[1].rsplit(s, 1)[0] for fs, s in ((r1files+r2files, '_L0'), (ddfiles, '_DD')) for x in fs)
    pfx = os.path.commonprefix(zs)
    assert pfx	# prefix is not a null string!
    cfg = { 'basename': pfx, 'ext': ext, 'dir': mydir, 'sampno': sampno, 'dataset_id': datasetID}
    if r1files+r2files:
        cfg['r1files'] = ' '.join(r1files)
        cfg['r2files'] = ' '.join(r2files)
        cfg['files'] = ' '.join(r1files+r2files)
    if ddfiles:
        cfg['dd1file'] = ddfiles[0]
        cfg['dd2file'] = ddfiles[1]
    config['DEFAULT'] = cfg
    if False:
        for pn, cs in config.items():
            print("  config part:", pn)
            for k in sorted(cs.keys()):
                print("    ", k,  '=', cs[k])
        print('did OMGpipe.main() - calling dorun.main()')
    return dorun.main([sys.argv[1]], config)

if __name__=="__main__":
    main()
