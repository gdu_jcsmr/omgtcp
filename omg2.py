#!/usr/bin/env python

#
# Example for ANU: list OMG datasets
#

from __future__ import print_function
import time
import subprocess
import ckanapi
import sys


def main():
    # set up a our CKAN remote API access
    base = 'https://data.bioplatforms.com'
    apikey = sys.argv[1]
    remote = ckanapi.RemoteCKAN(base, apikey=apikey)
    # show the organization. this will gives us some metadata on the organization
    # itself, and the packages (also called 'datasets')
    omg = remote.action.organization_show(id='bpa-omg', include_datasets=True)
    packages = omg['packages']
    print('%s has %d packages' % (omg['id'], len(packages)))

    #
    # if we want all the packages, and all the resources (the files under a package)
    # the easiest way is through the search API
    #

    # we set a high limit to avoid having to use paging
    limit = 50000

    results = remote.action.package_search(
        fq='+organization:bpa-omg',  # this is an Apache SOLR format query against the CKAN schema
        include_private=True,  # almost all the data is 'private' (authorization required to view)
        rows=limit)['results']
    for package in sorted(results, key=lambda p: (p['type'], ['id'])):
        # the package has all the relevant metadata for this group of files, including
        # contextual metadata (from the sample), sequencing metadata
        # it also has details for all resources in the package
        #
        # packages have a type: the type defines the schema in use. we generally model
        # the type according the technology and -omics in common; the metadata should
        # be that required by the relevant BPA workflow
        #
        # later on, you may find that doing a package_search against the 'type' field
        # is a good way to approach things (example below)
        print('listing package %s of type %s' % (package['id'], package['type']))
        for resource in package['resources']:
            # resources also have metadata: this should be metadata that may change
            # between files in the same package, plus the MD5 checksum and other
            # basic fields like the file URL
            fname = resource['url'].rsplit('/', 1)[-1]
            print('  %s  %s' % (fname, resource['md5']))

    # example search by package type: this is useful if you're applying different
    # logic to the various different types
    packages = remote.action.package_search(q='type:omg-genomics-hiseq', include_private=True, rows=limit)['results']
    print('genomics hiseq has %d packages' % len(packages))

    # to download a file, we simply have to send our API key in the Authorization header
    # you can use Python, but I recommend `wget` with the arguments provided as it'll
    # handle download resumption, etc: required for the extremely large files we have
    # for OMG (tens of gigabytes per file)
    download_package = remote.action.package_show(id='api-test-dataset')
    wget_args = [
        'wget',
        '--no-http-keep-alive',
        '--header=Authorization: %s' % (apikey),
        '-c',
        '-t', '0'
    ]
    wget_args.append(download_package['resources'][0]['url'])
    print('calling wget: %s' % (wget_args))
    subprocess.call(wget_args)

    # let's make a test package, and then upload a file into it (we'll just use the file
    # we downloaded previously)
    package = remote.action.package_create(
        name='test-upload-%d' % (time.time()),  # some unique name for the package (try to make it meaningful)
        title='Test Upload',  # descriptive (user-visible) title
        owner_org='anu-abc-upload',
        private=True  # require login to access
    )
    print("package created, browse contents at: https://data.bioplatforms.com/dataset/%s" % package['name'])
    # upload a file (a package can hold more than one file, but we'll just put one in for now
    with open('giphy.gif', 'rb') as fd:
        resource = remote.action.resource_create(
            package_id=package['id'],  # the ID of the package to add this file to
            name='typing cat',  # descriptive title
            upload=fd)


if __name__ == '__main__':
    main()
