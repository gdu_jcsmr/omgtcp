#!/bin/bash

WORKDIR=${WORKDIR:-/mnt/OMG}
[[ $PWD == $WORKDIR/* ]] && {
    echo Please change to $WORKDIR to run this script.
    exit 1
}
logfile=batch-$(date +%Y%m%d-%H%M%S).log
source ~/omg/code/param.sh

set -ex
for m in ~/omg/prog/macse*.jar
do
  macse=$m
done

# script to run all the target files for all the samples through muscle
# and then through iqtree

[ $# -eq 0 ] && echo "Please include path to SSH key file" && exit

#sshfile=~/.ssh/nectar
sshfile=$1

cd $WORKDIR
[ -d mktree ] && rm -r mktree
mkdir mktree
cd mktree

{
date
echo SHELL=$SHELL
echo Working in $PWD
set -x

# get remote sample data
# first parameter is the name of the SSH key 
[ $# -eq 1 ] && nodes="$(tail -n +2 ~/host-list | cut -f 2)"
# All other parameters are node names if passed in
[ $# -gt 1 ] && nodes="${@:2}"

# pull down remote diplotype files
mkdir -p tmp/f1 tmp/f2 FILES
for ip in $nodes
do
   scp -i $sshfile $ip:$WORKDIR/\*/\*_DD.D.fa tmp/f1/.
   for fn in tmp/f1/*
   do
     fn2=tmp/f2${fn#tmp/f1}
     # keep non-empty non-duplicate files
     [ -e $fn2 ] || [ -s $fn ] && mv $fn $fn2 || rm $fn
   done
done
mv tmp/f2/* FILES/.
rmdir tmp/f[12]
echo done fetching remote files.
# it might be good to limit the followingto sample directories
for fn in ../*/*_DD.D.fa 
do
  [ -s $fn ] || continue
  [ -s ${fn%/*_DD.D.fa}/protref.fa || continue # not a sample directory
  bn=${fn#../*/}
  [ -e FILES/$bn ] && rm FILES/$bn  # discard remote duplicate versions - use local version
  ln $fn FILES/$bn
done
# *** TEMPORARY ****
rm FILES/53336_* # exclude bat test sample
echo done connecting local files.

for tf in FILES/*_DD.D.fa
do
  fn=${tf##FILES/}
  bn=${fn%_DD.D.fa}
  for ext in 3p exon 5p
  do
    [ -d ${bn}-$ext ] && rm -rf ${bn}-$ext
  done
  # [ -d $bn ] && rm -rf $bn
  ~/omg/code/fasplit.py -p -c $bn $bn $tf
done


# the following uses multiple threads on one machine - it would be
# good to use multiple machines as macse is a bit slow.

declare -a tgts

date
z=exon
tgts=( $(for d in *-$z ; do ( cd $d ; ls ) ; done | sort -u ))
declare -i ntgts=${#tgts[*]}
echo $ntgts targets present
echo first target is $tgts
mx=aligned-$z
mkdir -p $mx
declare -i i tn threads=4
for (( tn=0; tn<threads; tn++ ))
do
  {
  echo Starting thread $tn
  # use available threads for parallel processing
  # for i in {$tn..$ntgts..4}
  for (( i=tn; i<ntgts; i+=threads ))
  do
    tgt=${tgts[$i]%.fa}
    echo
    echo target: $tgt
    cat *-$z/$tgt.fa >$tgt-tmp.fa
    rm *-$z/$tgt.fa	# remove processed files to save space
    # muscle $tgt-tmp.fa >$mx/$tgt-mx.fa
    java -jar $macse -prog alignSequences -seq $tgt-tmp.fa
    mv $tgt-tmp_NT.fa $mx/$tgt-mx.fa
    rm $tgt-tmp.fa $tgt-tmp_AA.fa
  done 
  } >step$tn.log 2>&1 &
  sleep 4
done
wait
cat step*.log ; rm step*.log
date

# the following might also be run using multiprocessing (like above)
for z in 3p 5p
do
  tgts=($( for d in *-$z; do (cd $d; ls); done | sort -u ))
  declare -i ntgts=${#tgts[*]}
  echo $ntgts targets present
  echo first target is $tgts
  mx=aligned-$z
  mkdir -p $mx
  for tgtx in ${tgts[*]}
  do
    echo
    tgt=${tgtx%.fa}
    echo target: $tgt
    cat *-$z/$tgt.fa | muscle >$mx/$tgt-mx.fa
  done
done
date

tgts=()

# get rid of work directories
#for tf in ../*/*_DD.D.fa
#do
#  fn=${tf##*/}
#  bn=${fn%_DD.D.fa}
#  [ -d ${bn} ] && rm -r ${bn}
#done

for z in exon 3p 5p
do
  mx=aligned-$z
  ~/omg/code/muscle-combine.py $mx .fa
  # output goes into $mx.fa

  # process $mx.fa
  iqtree -nt AUTO -s $mx.fa

  rm -r $mx
done
date
} >$logfile 2>&1
( echo; echo Job done. ) | mail -A $logfile -s "iqtree log file" $email
