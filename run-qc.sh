#!/bin/bash

function die {
  echo die: "$@"
  exit 1
}

[ $# -gt 0 -a $# -lt 3 ] || die "usage: $0 [-n] directory"

basedir=${WORKDIR:-/mnt/OMG}
codedir=$HOME/omg/code

source $codedir/param.sh # this file is installed by ansible

set -ex

kd=$PWD
eval wdir=\$$#	# get the last parameter (the directory)
if [ -d $wdir/raw ]
then
  cd $wdir
  raw=( $( find raw -name \*.fastq\* ) )
  echo Files: ${raw[@]}
  echo Found ${#raw[@]} raw fastq files
  # note: if no files are found there is an odd number of files in raw/
  (( ${#raw[@]} == 0 )) && die raw directory with no reads files
  (( ${#raw[@]} % 2 )) && die unpaired raw files
  fetched=false
else
  [ $wdir != . ] && {
    echo Create $wdir directory and fetch OMG sample raw read files.
    mkdir -p $wdir
    cd $wdir
  }
  date
  $codedir/omg-fetch.py $omgcode || die fetch failed!
  fetched=true
fi
echo -n Directory: ; /bin/pwd ;

cd $kd
date
~/omg/code/pipe-qc.ini "$@"
date
# Disabled until we've improved upload handling
#$fetched && {
#  set -ex
#  echo the following upload fails if you do not have permission to upload results to the Oz mammals data store.
#  $codedir/omg-put.py $omgcode || die upload failed.
#}
echo Done.
