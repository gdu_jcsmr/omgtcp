#!/bin/bash

die () { date ; echo "$@" ; exit 1 ; }

topdir=${WORKDIR:-/mnt/OMG}
codedir=$HOME/omg/code
log=batch.log

source $codedir/param.sh # this file is installed by ansible

[ -n "$1" ] || die usage $0 sample\[-datasetID]
if [ $1 = . ] ; then
   s=${PWD##*/}
   cd ..
else
   cd $topdir
   s=$1
fi

cat >SUBMIT/$s <<-CMDS
        # use /bin/sh syntax for die() function!
        die () { date ; echo "$@" ; exit 1 ; }

	set -e
	[ -d $s ] || mkdir $s || exit
        cd $s
	{ 
		date
		$codedir/run-qc.sh . || \
		  die "$s: QC pipeline failed!"
		date
		$codedir/pipe-rawmt.ini . || \
		  die "$s: rawmt pipeline failed!"
		date
	} >$log 2>&1
	echo Sample $s done. | mail -s "OMG: Sample $s done." -A $log $(cat ~/.forward)
	CMDS

if [ -f BATCH/* ] ; then
    echo BATCH/* is currently being processed.
    echo $s is scheduled to run later.
else
    echo start processing $s
    $codedir/cronjob.sh
fi
