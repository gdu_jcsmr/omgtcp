#!/usr/bin/env python3

import sys
import os
import argparse
from math import log2, floor
from statistics import median


def get_samples_targets_cov(in_dir, target_fns, normalise=False, read_length=100):
    """
        From a list of target filenames, return a dict 
        of samples each containing a dict [target] = coverage
        and a dict[target] = dict[sample] = cov
        If normalise is True, divide coverage by per-million sample reads.
    """
    samples_targets_cov = {}
    target_sample_cov = {}
    for tfn in target_fns:
        sample_name = tfn.split('_target')[0]
        samples_targets_cov[sample_name] = {}
        print('Reading coverage from', tfn, file=sys.stderr)
        total_reads = 0
        with open(os.path.join(in_dir,tfn)) as f:
            for line in f:
                if line.startswith('*'):
                    continue
                cols = line.strip().split('\t')
                target = cols[0]
                reads = int(cols[2])
                cov = reads*read_length / int(cols[1]) # reads in bp / target length
                total_reads += reads
                samples_targets_cov[sample_name][target] = cov
                if target not in target_sample_cov:
                    target_sample_cov[target] = {}
                target_sample_cov[target][sample_name] = cov

        if normalise:
            for target in samples_targets_cov[sample_name]:
                if target != 'unmapped':
                    samples_targets_cov[sample_name][target] *= 1000000/total_reads

    return samples_targets_cov, target_sample_cov


def get_target_table(target_sample_cov):
    """
        From a dictionary of targets 
        Return per-base target cover as a 2D table, where
        rows = samples, columns = targets
    """
    targets = set()
    samples = set()
    for target in target_sample_cov:
        for sample in target_sample_cov[target]:
            targets.add(target)
            samples.add(sample)

    samples_targets = []
    sample_order = []
    target_order = []
    for i, sample in enumerate(samples): 
        target_covs = []
        sample_order.append(sample)
        for target in targets:
            if i == 0:
                target_order.append(target)
            key = (target,sample)
            if sample in target_sample_cov[target]:
                target_covs.append(target_sample_cov[target][sample])
            else:
                target_covs.append(0)
        samples_targets.append(target_covs)

    return samples_targets, sample_order, target_order


def get_filtered_samples_table(target_table, sample_order, target_order, median_cov=5):
    """
        We're focusing on dubious or poor quality samples so,
        Keep samples with median coverage less than (log2) median_cov 
    """
    tt = target_table
    kept_rows =	[i for i,r in enumerate(tt) if median(r) < median_cov]
    kept_tt = [r for i,r in enumerate(tt) if i in kept_rows]
    kept_sample_order = [s for i,s in enumerate(sample_order) if i in kept_rows]
    return kept_tt, kept_sample_order, target_order


def get_filtered_targets_table(target_table, sample_order, target_order, median_cov=5):
    """
        We're focusing on dubious or poor quality targets so,
        Keep targets(columns) with median coverage less than (log2) median_cov
    """
    tt = target_table
    ttT = [[tt[j][i] for j in range(len(tt))] for i in range(len(tt[0]))]
    kept_cols = set([i for i,r in enumerate(ttT) if median(r) < median_cov])
    kept_tt = [r for i,r in enumerate(ttT) if i in kept_cols]
    kept_ttT = [[kept_tt[j][i] for j in range(len(kept_tt))] for i in range(len(kept_tt[0]))]
    kept_target_order = [t for i,t in enumerate(target_order) if i in kept_cols]
    return kept_ttT, sample_order, kept_target_order


def sort_target_table(target_table, sample_order, target_order, by_row=True):
    """
        Order from best to worst
    """
    if by_row:
        medcov = [(sum(r), i) for i,r in enumerate(target_table)]
        medcov_order = [i for c,i in sorted(medcov)]
        try:
            return target_table[medcov_order,:], sample_order[medcov_order], target_order
        except:
            print(target_table, medcov_order)
    else:
        #tt = np.array(target_table).T
        tt = [[tt[j][i] for j in range(len(tt))] for i in range(len(tt[0]))]
        medcov = [(sum(r), i) for i,r in enumerate(tt)]
        medcov_order = [i for c,i in sorted(medcov, reverse=True)]
        # return list(tt.T[:,medcov_order]), sample_order, target_order[medcov_order])
        return [[tt[j][i] for j in range(len(tt))] for i in range(len(tt[0]))][:,medcov_order], sample_order, target_order[medcov_order]


def binning(samples_targets_cov, num_bars=251, max_cov_range=1000, low_cov_limit=20, normalise=False):
    """
        Create the bar structures that will be used when plotting a barchart/histogram.
        Input: samples_targets_cov is a dict of dicts of cov
        Output: bins[bin] = [(sample,target,cov)], range = max_cov_range/(num_bars-1)
        Each bar is part of a histogram, so the height is frequency, bar widths span fixed ranges.
        Also return a special low-coverage bin to plot separately(?)
        If normalise then check which targets, if any, never appear in the top bin and report these.
    """
    range = max_cov_range/(num_bars-1)
    bins = {}
    low_cov_bin = []
    all_targets = set()
    good_targets = set()
    poor_targets = set()
    for sample in samples_targets_cov:
        for target in samples_targets_cov[sample]:
            good = False
            cov = samples_targets_cov[sample][target]
            if cov < low_cov_limit:
                low_cov_bin.append((sample, target, cov))
                poor_targets.add(target)
            else:
                good_targets.add(target)
            bin = floor(cov/range)
            if bin * range > max_cov_range:
                bin = floor(max_cov_range/range)
                good = True
            if bin not in bins:
                bins[bin] = []
            bins[bin].append((sample, target, cov))

    #print('poor:', len(poor_targets), list(poor_targets))
    #print('good:', len(good_targets), list(good_targets))
    #sys.exit()
    # bins[-1] = low_cov_bin
    poor_list = list(poor_targets.difference(good_targets))
    if normalise:
        print('These', len(poor_list), 'targets never made the "good" bin:')
    else:
        print('These', len(poor_list), 'targets never had more than', low_cov_limit, 'coverage:')
    print(poor_list)
    return bins, range
   

def build_bars(bins, range):
    """
        Input: bins[bin] = [(sample, target, cov)], range=size of each bin
        Output: x,y,text for bar plot:
        x = [bar name], y = [frequency], 
        text=[target:#samples it appears in, in this bin]
    """
    sample_set = set()
    for bin in bins:
        for s,t,c in bins[bin]:  # sample, target, count
            sample_set.add(s)
    num_samples = len(sample_set) 

    x, y, text = [], [], []   
    for i, bin in enumerate(sorted(bins)):
        bin_target_count = {}  # how often is this target seen in this bin
        for s,t,c in bins[bin]:  # sample, target, count
            if t not in bin_target_count:
                bin_target_count[t] = 0
            bin_target_count[t] += 1

        common_targets = sorted(bin_target_count, 
                key=bin_target_count.get, reverse=True)[:25]  # number of targets to list

        x.append((i+1)*range)
        y.append(len(bins[bin]))
        bar_text = []
        for c in common_targets:
            bar_text.append(str(c) + ':' + '{0:.2f}'.format(bin_target_count[c]*100/num_samples) +\
                    '% (' + str(bin_target_count[c]) +')')
        text.append('"' + '<br>'.join(bar_text) + '"')
    return x, y, text


def write_HTML_barchart(filename, x, y, text, xlabel, ylabel, title=''):
    """
       	Construct HTML file with embedded JS and data for barchart
    """
    with open(filename, 'w') as out:
        out.write('<html>\n<head>')
        out.write('    <script src="https://cdn.plot.ly/plotly-1.2.0.min.js">')
        out.write('</script>\n</head>\n')
        out.write('<body>\n')
#        out.write('    <h2>' + title + '</h2>\n')
        out.write('    <div id="barchart" style="width:1500px;height:800px;"></div>')
        out.write('    <script>\n')
        out.write('        var data = [\n')
        out.write('            {\n')
        out.write('                x: [' + ','.join(map(str,x)) + '],\n')
        out.write('                y: [' + ','.join(map(str,y)) + '],\n')
        out.write('                text: [' + ',\n'.join(text) + '],\n')
        out.write('                xaxis: ' + '"' + str(xlabel) + '"' + ',\n')
        out.write('                yaxis: ' + '"' + str(ylabel) + '"' + ',\n')
        out.write('                type: "bar"\n')
        out.write('            }\n')
        out.write('        ];\n')
        out.write('        var layout = {\n')
        out.write('            title: "' + title + '"' + ',\n')
        out.write('            xaxis: {\n')
        out.write('                text: "' + str(xlabel) + '"' + '\n')
        out.write('            },\n')
        out.write('            yaxis: {\n')
        out.write('                text: "' + str(ylabel) + '"' + '\n')
        out.write('            }\n')
        out.write('        }\n')
        out.write('        Plotly.newPlot("barchart", data, layout);\n')
        out.write('    </script>\n')
        out.write('</body>\n</html>\n')


def write_HTML_heatmap(filename, data, xlabel, ylabel, title=''):
    """
        Construct HTML file with embedded JS and data for heatmap
    """
    with open(filename, 'w') as out:
        out.write('<html>\n<head>')
        out.write('    <script src="https://cdn.plot.ly/plotly-1.2.0.min.js">')
        out.write('</script>\n</head>\n')
        out.write('<body>\n')
 #       out.write('    <h2>' + title + '</h2>\n')
        out.write('    <div id="heatmap" style="width:1500px;height:800px;"></div>')
        out.write('    <script>\n')
        out.write('        var data = [\n')
        out.write('            {\n')
        out.write('                z: ')
        rows = []
        for row in data:
            rows.append('[' + ','.join(map(str, row)) + ']')
        out.write('[' + ','.join(rows) + '],\n')
        out.write('                x: ' + str(xlabel) + ',\n')
        out.write('                y: ' + str(ylabel) + ',\n')
        out.write('                colorscale: [\n')
        out.write('                    ["0.0", "rgb(150,10,30)"],\n')
        out.write('                    ["0.03", "rgb(220,10,38)"],\n')
        out.write('                    ["0.05", "rgb(210,130,50)"],\n')
        out.write('                    ["0.10", "rgb(200,200,20)"],\n')
        out.write('                    ["0.15", "rgb(200,220,220)"],\n')
        out.write('                    ["0.20", "rgb(130,210,200)"],\n')
        out.write('                    ["0.25", "rgb(40,155,185)"],\n')
        out.write('                    ["0.5", "rgb(0,0,250)"],\n')
        out.write('                    ["1.0", "rgb(40,0,170)"],\n')
        out.write('                ],\n')
        out.write('                colorbar: {\n')
        out.write('                  tick0: 0,\n')
        out.write('                  tickmode: "array",\n')
        out.write('                  tickvals: [0, 10, 20, 30, 50, 100, 200]\n')
        out.write('                },\n')
        out.write('                zauto:false,\n')
        out.write('                zmin: 0,\n')
        out.write('                zmax : 200,\n')
        out.write('                type: "heatmap"\n')
        out.write('            }\n')
        out.write('        ];\n')
        out.write('        var layout = {\n')
        out.write('            title: \"'+ title +'\",\n')
        out.write('            titlefont: {\n')
        out.write('                size: 20\n')
        out.write('            },\n')
        out.write('            xaxis: {\n')
        out.write('                title: "Genomic target region",\n')
        out.write('                titlefont: {\n')
        out.write('                    size: 20\n')
        out.write('                },\n')
        out.write('                showticklabels: false\n')
        out.write('            },\n')
        out.write('            yaxis: {\n')
        out.write('                title: "Sample",\n')
        out.write('                titlefont: {\n')
        out.write('                    size: 20\n')
        out.write('                },\n')
        out.write('                showticklabels: false\n')
        out.write('            }\n')
        out.write('        };\n')
        out.write('        Plotly.newPlot("heatmap", data, layout);\n')
        out.write('    </script>\n')
        out.write('</body>\n</html>\n')


def main():
    """
        Collect Targets
        Collect all deduped target coverage in a directory
        as collected by samtools idxstats
        Present a histogram of all target coverage with separate bars
        for low_cut and high_cut coverge values.
        Take bottom 5% and top 5% and present box plots of these
        Cluster the results and save as html/JS
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', help='Path to sample and target coverage info')
    parser.add_argument('--low_cut', type=int, help='Targets with coverage up-to '+\
            'and including this value will be presented in their own bar')
    parser.add_argument('--high_cut', type=int, help='Targets with coverage down-to '+\
            'and including this value will be presented in their own bar')
    parser.add_argument('--rpm', action='store_true', help='Normalise coverage to '+\
            'reads-per-million')
    parser.add_argument('--read_length', type=int, default=100, help='Number of bases per read-end')
    parser.add_argument('--median_cov', type=int, default=5, help='Median low coverage for heatmap')
    args = parser.parse_args()
    in_dir = args.input_dir
    dn = in_dir.split('/')[-1]

    # Coverage file
    target_fns = [f for f in os.listdir(in_dir) if '_targets.txt' in f]
    print (len(target_fns), 'target files being read', file=sys.stderr)
    samples_targets_cov, target_sample_cov = get_samples_targets_cov(in_dir, target_fns, 
            normalise=args.rpm, read_length=args.read_length)

    # histogram
    bins, range = binning(samples_targets_cov, normalise=args.rpm)
    print (len(bins), range)
    x, y, text = build_bars(bins, range)
    dn = in_dir.split('/')[-1]
    if not args.rpm:
        filename = dn + '_raw_histogram.html'
        title = 'Raw coverage histogram'
    else:
        filename = dn + '_rpm_histogram.html'
        title = 'RPM coverage histogram'
    xlabel = 'Target coverage'
    ylabel = 'Frequency observed'
    write_HTML_barchart(filename, x, y, text, xlabel, ylabel, title=title)


    target_table, sample_order, target_order = get_target_table(target_sample_cov)
    filtered_samples_table, fs_sample_order, fs_target_order =\
            get_filtered_samples_table(target_table, sample_order, target_order, median_cov=args.median_cov)
    filtered_targets_table, ft_sample_order, ft_target_order =\
            get_filtered_targets_table(target_table, sample_order, target_order, median_cov=args.median_cov)    
    filtered_table, f_sample_order, f_target_order =\
            get_filtered_samples_table(filtered_targets_table, ft_sample_order, ft_target_order, median_cov=args.median_cov*2/3)

    # all samples and targets
    all_fn = '_'.join([dn, 'cov_heatmap_all.html'])
    target_table, sample_order, target_order =\
            sort_target_table(target_table, sample_order, target_order, by_row=True)
    target_table, sample_order, target_order =\
            sort_target_table(target_table, sample_order, target_order, by_row=False)    
    write_HTML_heatmap(all_fn, target_table, target_order, sample_order, 
            title=dn + ' median coverage, all samples and targets')

    # filtered samples and all targets
    fs_fn = '_'.join([dn, 'cov_heatmap_filt_samples.html'])
    filtered_samples_table2, fs_sample_order2, fs_target_order2 =\
            sort_target_table(filtered_samples_table, fs_sample_order, fs_target_order, by_row=True)
    filtered_samples_table3, fs_sample_order3, fs_target_order3 =\
            sort_target_table(filtered_samples_table2, fs_sample_order2, fs_target_order2, by_row=False)
    write_HTML_heatmap(fs_fn, filtered_samples_table3, fs_target_order3, fs_sample_order3, 
            title=dn + ' median coverage, filtered samples and all targets. Cutoff ' + str(args.median_cov))

    # all samples and filtered targets
    ft_fn = '_'.join([dn, 'cov_heatmap_filt_targets.html'])
    filtered_targets_table2, ft_sample_order2, ft_target_order2 =\
            sort_target_table(filtered_targets_table, ft_sample_order, ft_target_order, by_row=True)
    filtered_targets_table3, ft_sample_order3, ft_target_order3 =\
            sort_target_table(filtered_targets_table2, ft_sample_order2, ft_target_order2, by_row=False)
    write_HTML_heatmap(ft_fn, filtered_targets_table3, ft_target_order3, ft_sample_order3, 
            title=dn + ' median coverage, all samples and filtered targets. Cutoff ' + str(args.median_cov))

    # filtered samples and filtered targets
    f_fn = '_'.join([dn, 'cov_heatmap_filt.html'])
    filtered_table2, f_sample_order2, f_target_order2 =\
            sort_target_table(filtered_table, f_sample_order, f_target_order, by_row=True)
    filtered_table3, f_sample_order3, f_target_order3 =\
            sort_target_table(filtered_table2, f_sample_order2, f_target_order2, by_row=False)
    write_HTML_heatmap(f_fn, filtered_table3, f_target_order3, f_sample_order3, 
            title=dn + ' median coverage, filtered samples and filtered targets. Cutoff ' + str(args.median_cov*2/3))


if __name__ == '__main__':
    main()
