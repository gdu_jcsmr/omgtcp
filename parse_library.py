#!/usr/bin/env python3

import sys


def return_metrics(fn):
    with open(fn, 'r') as f:
        while (True):
            try:
                line = next(f)
            except StopIteration:
                break
            if line.startswith('## METRICS'):
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                perc_dup_hdr = cols[8]
                est_lib_size_hdr = cols[9]
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                d = {'sample':fn.split('.')[0],
                     perc_dup_hdr:"{0:.2f}".format(float(cols[8])*100),
                     est_lib_size_hdr:int(cols[9])}
                return d
    return {'sample':fn.split('.')[0]}


def main():
    print (return_metrics(sys.argv[1]))     


if __name__ == '__main__':
    main()
