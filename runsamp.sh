#!/bin/bash

[ -n "$1" ] || { echo usage: $0 sampleID '[bat|marsupial]' ; exit 1 ; }

topdir=${WORKDIR:-/mnt/OMG}
codedir=$HOME/omg/code

if [ $1 = . ] ; then
   s=${PWD##*/}
   cd ..
   [[ $PWD = $topdir ]] || die Run in a sub-directory of $topdir ... 
else
   cd $topdir
   s=$1	# should check for a valid sampleID or sampleID-datasetID
fi

cat >SUBMIT/$s <<-CMD
	export WORKDIR=$topdir
	cd $WORKDIR
	$codedir/run-omgpipeline.sh $s $2
	CMD

if [ -f BATCH/* ] ; then
    echo Sample $( cd BATCH; echo BATCH/*) is currently being processed.
    echo $s is scheduled to run later.
else
    echo Start processing $s
    $codedir/cronjob.sh
fi
