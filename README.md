#Scripts/code for OMG TCP

This repository contains scripts that are setup to process a sample from the OZ-mammals project (see [http://www.bioplatforms.com/oz-mammals/]()).

Currently, documentation for the project (a description of the purpose) is in the ABC's Labnotes archive.

The URL for the remote git repository is [http://bitbucket.org/gdu_jcsmr/omgtcp.git]()

Releases of the pipeline code are tagged in the repository:

- v0.1.a.1 Initial release, 22/12/2017 (commit fec42ab98d7ee17)
- v0.1.a.2 Second alpha release, 2/1/2018 (commit fac67e2b59d31b7)
- v0.2.a Improved exon identification and processing ... also added iqtree processing of multiple samples.

The latest version is in the HEAD or master branch on the repository.
