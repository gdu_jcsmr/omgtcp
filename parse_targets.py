#!/usr/bin/env python3

import sys

"""
    Read targets file and return the sum of mapped reads
"""

def return_metrics(fn):
    mapped_reads = 0
    unmapped_reads = 0
    with open(fn, 'r') as f:
        for line in f:
            cols = line.strip().split('\t')           
            if line.startswith('*'):
               unmapped_reads = int(cols[2])
            mapped_reads += int(cols[2])
    if unmapped_reads == 0:
        ratio_reads = '+INF'
    else:
        ratio_reads = mapped_reads/unmapped_reads

    d = {'Mapped reads(DNA)':mapped_reads//2, 
         'Unmapped reads(DNA)':unmapped_reads//2,
         'Mapped/unmapped ratio':ratio_reads} 
    return d

def main():
    print(return_metrics(sys.argv[1]))


if __name__ == '__main__':
    main()
