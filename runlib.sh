# Some shell functions for OMG scripts
function die {
    echo "$@" >&2
    exit 1
}

function range {
  # gives a list of numbers - a bit like python's range function
  # takes one or two arguments
  # 2nd argument is start value (default 0)
  let i=0
  [[ "$2" != '' ]] && let i=$2
  while (( $i<$1 ))
  do
    echo $i
    let i=i+1
  done
}

# add a prefix to file names - allowing for directories
function pref {
  local p=$1
  shift
  for fn
  do
    local suff=${fn##*/}
    echo ${fn%$suff}$p$suff
  done
}


# dorun() will run a command if the result file
# (which is given both as the first argument
# and in the command) does not already exist

# so if you would normally run a command like: $ cmd x y -o z p q r
# in your script, where 'z' is the output file name, then you can use
# the following command instead:
#   $ dorun z cmd x y -o z p q r
# and the script will skip this step if command ran to completion and
# created the z result file previously.
# if you have several result files, then you only need to choose one as the 'indicator' file for the command

# you can do "$ dorun x cmd infile '>x' or "$ dorun x cmd infile \> x" ...
# it is possible that "$ dorun x cmd out=x y z" will work as needed.

# Note: don't use dorun() where there is no obvious output file.
# Instead, write the following:
# $ check x cmd y z
# will create an empty file called x when/if "$ cmd y z" completes successfully 

function dorun {
  local bashflags=$-
  set +x
  prefix=zzz-	# make these files appear last in a ls(1) listing
  local vcmd=false # verbose?
  local date= # do date command before & after
  local time= # time the command
  local eflag=-f rename=true check= darg=-f
  while getopts "p:c:vxdtsrD" opt ; do
    # echo process opt=$opt OPTARG=$OPTARG
    case $opt in
      p) prefix=$OPTARG ;; # allow a different temporary file prefix
      c) check=$OPTARG ;;
      v) vcmd=true ;; # verbose - display actual command
      x) set -x ;;
      d) date=date ;;
      t) time=time ;;
      s) eflag=-s ;;
      r) rename=false ;; # don't rename for processing
      D) darg=-d ;;
      \?) die "Invalid option: -$OPTARG" ;;
      :) die "Option -$OPTARG requires an argument." ;;
    esac
  done
  shift $(($OPTIND - 1))
  OPTIND=1

    ## output is the same as the command ... that's just silly
  [ "$1" = "$2" ] && die "command name ($2) cannot be the output."

  # set variables initially
  local out="$1"
  shift 1
  if [ -z "$check" ] ; then
    local outx="$out" # use -c option when actual output has a suffix
  else
    local outx="$check"
  fi
  [ $darg "$outx" ] && return	# already done if output files exist

  local dst=$(pref $prefix "$out")

  # echo in dorun'()' ... processed options
  # echo target:\  $out
  # echo command: "$@"

  local -a cmd
  cmd=( "$@" )


  # step through the arguments, replace the $out name where it occurs
  # count the number of replacements, report if there are none

  local -i x cnt=0
  if $rename ; then
    # note: the replacement has the same extention ... in case that matters
    for (( x=1; x<${#cmd[@]}; x++))
    do
       [ "${cmd[$x]}" = "$dst" ] && die "Problem: tmpname $dst is also an argument"
       if [ "${cmd[$x]}" = "$out" ] || [ "${cmd[$x]}" = ">$out" ] || [[ "${cmd[$x]}" = "*=$out" ]] ; then
          cmd[$x]="${cmd[$x]%$out}$dst"
          let cnt+=1
       fi
    done
    (( "$cnt">0 )) || die "Argument \"$out\" not in command."
  fi
  local z f
  $vcmd && { # verbose?
    echo cmd[\@]=\( "${cmd[@]}" \) "    " \$\#cmd=${#cmd[@]} "    " \$\#=$#
  }

  # run the command ... and move the output if the command succeeded
  $date
  $time "${cmd[@]}" && {
    if $rename ; then
      for f in "$dst"* ; do # cater for using prefixes
        if [ $eflag $f -o $darg $f ] ; then
          # $out${f#$dst} constructs the full target name
          $vcmd && echo mv "$f" "$out${f#$dst}"
          mv "$f" "$out${f#$dst}"
        else
          die Empty or missing output: $f
        fi
      done
    fi
  }
  $date
  set -$bashflags # restore bash flags
}

function isfile {
  local f
  for f
  do
    [ -r "$f" ] || die "File $f cannot be read."
  done
  return 0
}

function check {
  (( $#>1 )) || die "usage: $0 dummyfile cmd ..."
  [ -f "$1" ] || {
    local fx="$1"
    shift
    "$@" || die Command failed - status=$?
    touch "$fx"
  }
}

function _agest {
  [[ "$1" == "-[no]t" ]] || die Internal error: bad operator = "$1".
  local op="$1"
  shift

  fx="$1"
  shift
  for f
  do
    [ "$f" $op "$fx" ] && fx="$f"
  done
  echo "$fx"
}

function newest {
  (( $#>0 )) || die "usage: $0 filenames ..."
  _agest -nt "$@"
  return
}

function oldest {
  (( $#>0 )) || die "usage: $0 filenames ..."
  _agest -ot "$@"
  return
}
