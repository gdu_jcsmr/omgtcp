#!/usr/bin/env python

import json
import os
import argparse


def main():
    """ combine dictionary entries from separate JSON files into a single dict and output to JSON """
    parser = argparse.ArgumentParser()
    parser.add_argument('jsondir', help='Path to directory of per-sample JSON quality files')
    parser.add_argument('outname', help='Name of combined JSON output file')
    args = parser.parse_args()

    qual_info = {}

    flist = [os.path.join(args.jsondir,f) for f in os.listdir(args.jsondir) if f.lower().endswith('.json')]
    for fn in flist:
        with open(fn, 'rt') as f:
            qual_info.update(json.load(f))

    with open(args.outname, 'wt') as out:
        json.dump(qual_info, out, indent=4)


if __name__ == '__main__':
    main()
