#!/bin/bash

###
# TCP Sample Quality Pipeline
# By Cameron Jack, ANU Bioinformatics Consultancy, 2018
# For the Oz Mammals Genomics Initiative
#
# This pipeline runs on a single sample from a collection of samples.
# After running, scripts pool the results and provide summary statistics
# for the overall quality of the samples following sequencing.
#
# The FETCH stage must have run before this
###

topdir=${WORKDIR:-/mnt/OMG}
codedir="${HOME}/omg/code"
datadir="${HOME}/omg/data"
progdir="${HOME}/omg/prog"

SAMPLE=$1

# set protein and DNA target references
refbn=protref
ref=${refbn}.fa
refidx=${refbn}.dmnd
PREF=$refidx
DREF=${ref/prot/dna}
echo Setting references to $PREF and $DREF

sampledir="$topdir/$SAMPLE"
qcworking="qcWorking"
THREADS=4

# Must be run from sampledir!
cd $sampledir
mkdir -p $qcworking

# combine lanes if required
if [[ $(ls raw/${SAMPLE}_*_R1*.gz | wc -l) > 1 ]] && [[ ! -f ${qcworking}/${SAMPLE}_R1.fq.gz ]] ; then
    if [[ ! -f ${qcworking}/${SAMPLE}_R1.fq.gz ]] ; then
        if [[ -f ${qcworking}/x_${SAMPLE}_R1.fq.gz ]] ; then
            rm ${qcworking}/x_${SAMPLE}_R1.fq.gz
        fi
        zcat raw/${SAMPLE}_*_R1*.gz | pigz \
            -p $THREADS >> ${qcworking}/x_${SAMPLE}_R1.fq.gz
        if [ $? -ne 0 ] ; then
            exit 1
        fi
        if [[ $(zcat ${qcworking}/x_${SAMPLE}_R1.fq.gz | head -n 8 | wc -l) -le 4 ]] ; then
            echo "Empty FASTQ!"
            exit 1
        fi
        mv ${qcworking}/x_${SAMPLE}_R1.fq.gz ${qcworking}/${SAMPLE}_R1.fq.gz
    fi
    if [[ ! -f ${qcworking}/${SAMPLE}_R2.fq.gz ]] ; then
        if [[ -f ${qcworking}/x_${SAMPLE}_R2.fq.gz ]] ; then
            rm ${qcworking}/x_${SAMPLE}_R2.fq.gz
        fi
        zcat raw/${SAMPLE}_*_R2*.gz | pigz \
            -p $THREADS >> ${qcworking}/x_${SAMPLE}_R2.fq.gz
        if [ $? -ne 0 ] ; then
            exit 1
        fi
        if [[ $(zcat ${qcworking}/x_${SAMPLE}_R2.fq.gz | head -n 8 | wc -l) -le 4 ]] ; then
            echo "Empty FASTQ!"
            exit 1
        fi
        mv ${qcworking}/x_${SAMPLE}_R2.fq.gz ${qcworking}/${SAMPLE}_R2.fq.gz
    fi
else # just link to the raw FASTQs
    ln raw/${SAMPLE}*_L001_R1_001.fastq.gz ${qcworking}/${SAMPLE}_R1.fq.gz
    ln raw/${SAMPLE}*_L001_R2_001.fastq.gz ${qcworking}/${SAMPLE}_R2.fq.gz
fi

# Align DNA reads to DNA reference with bbmap in "fast" mode
if [[ -f ${qcworking}/${SAMPLE}.bam ]] ; then
    rm ${qcworking}/${SAMPLE}.bam
fi
echo $progdir/bbmap/bbmap.sh -Xmx8g in=${qcworking}/${SAMPLE}_R1.fq.gz in2=${qcworking}/${SAMPLE}_R2.fq.gz \
    ref=$DREF t=4 out=${qcworking}/${SAMPLE}.bam fast=t unpigz=t nodisk=t
$progdir/bbmap/bbmap.sh -Xmx8g in=${qcworking}/${SAMPLE}_R1.fq.gz in2=${qcworking}/${SAMPLE}_R2.fq.gz \
    ref=$DREF t=4 out=${qcworking}/${SAMPLE}.bam fast=t unpigz=t nodisk=t


if [[ -f ${qcworking}/${SAMPLE}_sorted.bam ]] ; then
    rm ${qcworking}/${SAMPLE}_sorted.bam
fi
samtools view -bh ${qcworking}/${SAMPLE}.bam | samtools sort -@ ${THREADS} \
    -m 1G -o ${qcworking}/${SAMPLE}_sorted.bam -T ${SAMPLE}
samtools index ${qcworking}/${SAMPLE}_sorted.bam


# collect duplication level details
if [[ -f ${qcworking}/${SAMPLE}_library.txt ]] ; then
    rm ${qcworking}/${SAMPLE}_library.txt
fi
java -jar -Xmx6g ${progdir}/picard.jar EstimateLibraryComplexity \
    VALIDATION_STRINGENCY=SILENT INPUT=${qcworking}/${SAMPLE}_sorted.bam \
    O=${qcworking}/${SAMPLE}_library.txt R=$DREF


# deduplicate
if [[ -f ${qcworking}/${SAMPLE}_dedup.bam ]] ; then
    rm ${qcworking}/${SAMPLE}_dedup.bam
fi
java -jar -Xmx6g ${progdir}/picard.jar MarkDuplicates \
    VALIDATION_STRINGENCY=SILENT INPUT=${qcworking}/${SAMPLE}_sorted.bam \
    O=${qcworking}/${SAMPLE}_dedup.bam REMOVE_DUPLICATES=true ASSUME_SORT_ORDER=coordinate \
    M=${qcworking}/${SAMPLE}_dup_metrics.txt
samtools index ${qcworking}/${SAMPLE}_dedup.bam


# output filtered,deduped FASTQ
if [[ -f ${qcworking}/${SAMPLE}_dedup_R1.fq.gz ]] ; then
    rm ${qcworking}/${SAMPLE}_dedup_R1.fq.gz
    rm ${qcworking}/${SAMPLE}_dedup_R2.fq.gz
fi
samtools fastq -1 ${qcworking}/${SAMPLE}_dedup_R1.fq -2 ${qcworking}/${SAMPLE}_dedup_R2.fq \
    ${qcworking}/${SAMPLE}_dedup.bam
gzip ${qcworking}/${SAMPLE}_dedup_R1.fq
gzip ${qcworking}/${SAMPLE}_dedup_R2.fq


### Run these on deduped BAMs

# run diamond to get a proxy for protein read hits - takes 10 mins, doesn't add much
#if [[ ! -f ${qcworking}/${SAMPLE}_R1_dmnd.txt || ! -s ${qcworking}/${SAMPLE}_R1_dmnd.txt ]] ; then 
#    ${progdir}/diamond blastx -p ${THREADS} -e 1E-9 -f 6 --db ${PREF} \
#        -q ${qcworking}/${SAMPLE}_dedup_R1.fq.gz -o ${qcworking}/${SAMPLE}_R1_dmnd.txt
#    ${progdir}/diamond blastx -p ${THREADS} -e 1E-9 -f 6 --db ${PREF} \
#        -q ${qcworking}/${SAMPLE}_dedup_R2.fq.gz -o ${qcworking}/${SAMPLE}_R2_dmnd.txt
#fi

# Picard WGS metrics
if [[ -f ${qcworking}/${SAMPLE}_wgs.txt ]] ; then
    rm ${qcworking}/${SAMPLE}_wgs.txt
fi
java -jar -Xmx6g ${progdir}/picard.jar CollectWgsMetrics \
    VALIDATION_STRINGENCY=SILENT INPUT=${qcworking}/${SAMPLE}_dedup.bam \
    O=${qcworking}/${SAMPLE}_wgs.txt R=$DREF

# Picard fragment size metrics
if [[ -f ${qcworking}/${SAMPLE}_insert.txt ]] ; then
    rm ${qcworking}/${SAMPLE}_insert.txt
fi
java -jar -Xmx6g $progdir/picard.jar CollectInsertSizeMetrics \
    VALIDATION_STRINGENCY=SILENT INPUT=${qcworking}/${SAMPLE}_dedup.bam \
    O=${qcworking}/${SAMPLE}_insert.txt R=$DREF QUIET=true\
    M=0.5 H=${qcworking}/${SAMPLE}_insert.pdf


### Per target stats
if [[ -f ${qcworking}/${SAMPLE}_targets.txt ]] ; then
    rm ${qcworking}/${SAMPLE}_targets.txt
fi
samtools idxstats ${qcworking}/${SAMPLE}_dedup.bam > ${qcworking}/${SAMPLE}_targets.txt


# Write to JSON
${codedir}/output-sample-qual.py $qcworking $SAMPLE
