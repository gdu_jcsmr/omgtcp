#!/usr/bin/env python3

import matplotlib
matplotlib.use('Agg')
import sys
import os
import numpy as np
#from matplotlib import pyplot as plt
from seaborn import clustermap
import pandas as pd
import argparse
from math import log2, floor


def get_samples_targets_cov(target_fns, normalise=False):
    """
        From a list of target filenames, return a dict 
        of samples each containing a dict [target] = coverage
        If normalise is True, divide coverage by per-million sample reads.
    """
    samples_targets_cov = {}
    for tfn in target_fns:
        sample_name = tfn.split('_target')[0]
        samples_targets_cov[sample_name] = {}
        print('Reading coverage from', tfn)
        total_reads = 0
        with open(os.path.join(sys.argv[1],tfn)) as f:
            for line in f:
                if line.startswith('*'):
                    continue
                cols = line.strip().split('\t')
                target = cols[0]
                cov = int(cols[2])*100 / (int(cols[1])) # reads in bp / target length
                total_reads += cov
                samples_targets_cov[sample_name][target] = cov

        if normalise:
            for target in samples_targets_cov[sample_name]:
                if target != 'unmapped':
                    samples_targets_cov[sample_name][target] *= 1000000/total_reads

    return samples_targets_cov


def get_target_table(target_sample_cov):
    """
        From a dictionary of targets 
        Return per-base target cover as a 2D table, where
        rows = samples, columns = targets
    """
    targets = set()
    samples = set()
    for target, sample in target_sample_cov:
        targets.add(target)
        samples.add(sample)
       
    samples_targets = []
    sample_order = []
    target_order = []
    for i, sample in enumerate(samples): 
        target_cov = []
        sample_order.append(sample)
        for target in targets:
            if i == 0:
                target_order.append(target)
            target_cov.append(target_sample_cov[(target,sample)])
        samples_targets.append(target_cov)
            
    return samples_targets, sample_order, target_order


def get_filtered_samples_table(target_table, sample_order, target_order, median_cov=5):
    """
        We're focusing on dubious or poor quality samples so,
        Keep samples with median coverage less than (log2) median_cov 
    """
    tt = np.array(target_table)
    kept_rows =	[i for i,r in enumerate(tt) if np.median(r) < median_cov]
    kept_tt = [r for i,r in enumerate(tt) if i in kept_rows]
    kept_sample_order = [s for i,s in enumerate(sample_order) if i in kept_rows]
    return kept_tt, kept_sample_order, target_order


def get_filtered_targets_table(target_table, sample_order, target_order, median_cov=5):
    """
        We're focusing on dubious or poor quality targets so,
        Keep targets(columns) with median coverage less than (log2) median_cov
    """
    tt = np.array(target_table)
    kept_cols = set([i for i,r in enumerate(tt.T) if np.median(r) < median_cov])
    kept_tt = np.array([r for i,r in enumerate(tt.T) if i in kept_cols])
    kept_target_order = [t for i,t in enumerate(target_order) if i in kept_cols]
    return list(kept_tt.T), sample_order, kept_target_order


def sort_target_table(target_table, sample_order, target_order, by_row=True):
    """
        Order from best to worst
    """
    if by_row:
        medcov = [(np.sum(r), i) for i,r in enumerate(target_table)]
        medcov_order = np.array([i for c,i in sorted(medcov)])
        try:
            return list(np.array(target_table)[medcov_order,:]), list(np.array(sample_order)[medcov_order]), target_order
        except:
            print(np.array(target_table), medcov_order)
    else:
        tt = np.array(target_table).T
        medcov = [(np.sum(r), i) for i,r in enumerate(tt)]
        medcov_order = [i for c,i in sorted(medcov, reverse=True)]
        return list(tt.T[:,medcov_order]), sample_order, list(np.array(target_order)[medcov_order])


def binning(samples_targets_cov, num_bars=41, max_cov_range=1000, low_cov_limit=15):
    """
        Create the bar structures that will be used when plotting a barchart/histogram.
        Input: samples_targets_cov is a dict of dicts of cov
        Output: bins[bin] = [(sample,target,cov)], range = max_cov_range/(num_bars-1)
        Each bar is part of a histogram, so the height is frequency, bar widths span fixed ranges.
        Also return a special low-coverage bin to plot separately
    """
    range = max_cov_range/(num_bars-1)
    bins = {}
    low_cov_bin = []
    for sample in samples_targets_cov:
        for target in samples_targets_cov[sample]:
            cov = samples_targets_cov[sample][target]
            if cov < low_cov_limit:
                low_cov_bin.append((sample, target, cov))
            bin = floor(cov/range)
            if bin * range > max_cov_range:
                bin = floor(max_cov_range/range)
            if bin not in bins:
                bins[bin] = []
            bins[bin].append((sample, target, cov))
    return bins, range
   

def build_bars(bins, range):
    """
        Input: bins[bin] = [(sample, target, cov)], range=size of each bin
        Output: x,y,text for bar plot:
        x = [bar name], y = [frequency], 
        text=[target:#samples it appears in, in this bin]
    """
    sample_set = set()
    for bin in bins:
        for s,t,c in bins[bin]:  # sample, target, count
            sample_set.add(s)
    num_samples = len(sample_set) 

    x, y, text = [], [], []   
    for i, bin in enumerate(sorted(bins)):
        bin_target_count = {}  # how often is this target seen in this bin
        for s,t,c in bins[bin]:  # sample, target, count
            if t not in bin_target_count:
                bin_target_count[t] = 0
            bin_target_count[t] += 1
        
        common_targets = sorted(bin_target_count, 
                key=bin_target_count.get, reverse=True)[:10]
        
        x.append((i+1)*range)
        y.append(len(bins[bin]))
        bar_text = []
        for c in common_targets:
            bar_text.append(str(c) + ':' + '{0:.2f}'.format(bin_target_count[c]*100/num_samples) +\
                    '% (' + str(bin_target_count[c]) +')')
        text.append('"' + '<br>'.join(bar_text) + '"')
        
    return x, y, text


def write_HTML_barchart(filename, x, y, text, xlabel, ylabel, title=''):
    """
       	Construct HTML file with embedded JS and data for barchart
    """
    with open(filename, 'w') as out:
        out.write('<html>\n<head>')
        out.write('    <script src="https://cdn.plot.ly/plotly-1.2.0.min.js">')
        out.write('</script>\n</head>\n')
        out.write('<body>\n')
        out.write('    <h2>' + title + '</h2>\n')
        out.write('    <div id="barchart" style="width:1700px;height:1000px;"></div>')
        out.write('    <script>\n')
        out.write('        var data = [\n')
        out.write('            {\n')
        out.write('                x: [' + ','.join(map(str,x)) + '],\n')
        out.write('                y: [' + ','.join(map(str,y)) + '],\n')
        out.write('                text: [' + ',\n'.join(text) + '],\n')
        out.write('                xaxis: ' + '"' + str(xlabel) + '"' + ',\n')
        out.write('                yaxis: ' + '"' + str(ylabel) + '"' + ',\n')
        out.write('                type: "bar"\n')
        out.write('            }\n')
        out.write('        ];\n')
        out.write('        var layout = {\n')
        out.write('            title: "' + title + '"' + ',\n')
        out.write('            xaxis: {\n')
        out.write('                text: "' + str(xlabel) + '"' + '\n')
        out.write('            },\n')
        out.write('            yaxis: {\n')
        out.write('                text: "' + str(ylabel) + '"' + '\n')
        out.write('            }\n')
        out.write('        }\n')
        out.write('        Plotly.newPlot("barchart", data, layout);\n')
        out.write('    </script>\n')
        out.write('</body>\n</html>\n')


def write_HTML_heatmap(filename, data, xlabel, ylabel, title=''):
    """
        Construct HTML file with embedded JS and data for heatmap
    """
    with open(filename, 'w') as out:
        out.write('<html>\n<head>')
        out.write('    <script src="https://cdn.plot.ly/plotly-1.2.0.min.js">')
        out.write('</script>\n</head>\n')
        out.write('<body>\n')
        out.write('    <h2>' + title + '</h2>\n')
        out.write('    <div id="heatmap" style="width:2400px;height:1050px;"></div>')
        out.write('    <script>\n')
        out.write('        var data = [\n')
        out.write('            {\n')
        out.write('                z: ')
        rows = []
        for row in data:
            rows.append('[' + ','.join(map(str, row)) + ']')
        out.write('[' + ','.join(rows) + '],\n')
        out.write('                x: ' + str(xlabel) + ',\n')
        out.write('                y: ' + str(ylabel) + ',\n')
        out.write('                type: "heatmap"\n')
        out.write('            }\n')
        out.write('        ];\n')
        out.write('        Plotly.newPlot("heatmap",data);\n')
        out.write('    </script>\n')
        out.write('</body>\n</html>\n')


def main():
    """
        Collect Targets 2
        Collect all deduped target coverage in a directory
        as collected by samtools idxstats
        Present a histogram of all target coverage
        Take bottom 5% and top 5% and present box plots of these
        Cluster the results and save as html/JS
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', help='Path to sample and target coverage info')
    args = parser.parse_args()
    in_dir = args.input_dir
    dn = in_dir.split('/')[-1]

    # Coverage file
    target_fns = [f for f in os.listdir(in_dir) if '_targets.txt' in f]
    print (target_fns)
    samples_targets_cov = get_samples_targets_cov(target_fns)
    
    # non-normalised histogram
    bins, range = binning(samples_targets_cov)
    print (len(bins), range)
    x, y, text = build_bars(bins, range)
    filename = 'rawcov_histogram.html'
    xlabel = 'Target coverage'
    ylabel = 'Frequency observed'
    title = 'Raw coverage histogram'
    write_HTML_barchart(filename, x, y, text, xlabel, ylabel, title=title)

    # normalised histogram
    bins, range = binning(samples_targets_cov)
    x, y, text = build_bars(bins, range)
    filename = 'normcov_histogram.html'
    xlabel = 'Target coverage, normalised coverage per million sample reads'
    ylabel = 'Frequency	observed'
    title = 'Coverage histogram, normalised coverage per million sample reads'
    write_HTML_barchart(filename, x, y, text, xlabel, ylabel, title=title)

    sys.exit()

    target_table, sample_order, target_order = get_target_table(target_sample_cov)
    filtered_samples_table, fs_sample_order, fs_target_order =\
            get_filtered_samples_table(target_table, sample_order, target_order, median_cov=args.mediancov)
    filtered_targets_table, ft_sample_order, ft_target_order =\
            get_filtered_targets_table(target_table, sample_order, target_order, median_cov=args.mediancov)    
    
    # all samples and targets
    all_fn = '_'.join(['median_cov', dn, 'all.html'])
    target_table, sample_order, target_order =\
            sort_target_table(target_table, sample_order, target_order, by_row=True)
    target_table, sample_order, target_order =\
            sort_target_table(target_table, sample_order, target_order, by_row=False)    
    write_HTML_heatmap(all_fn, target_table, target_order, sample_order, 
            title=dn + ' median coverage, all samples and targets')

    # filtered samples and all targets
    fs_fn = '_'.join(['median_cov', dn, 'filt_samples.html'])
    filtered_samples_table2, fs_sample_order2, fs_target_order2 =\
            sort_target_table(filtered_samples_table, fs_sample_order, fs_target_order, by_row=True)
    filtered_samples_table3, fs_sample_order3, fs_target_order3 =\
            sort_target_table(filtered_samples_table2, fs_sample_order2, fs_target_order2, by_row=False)
    write_HTML_heatmap(fs_fn, filtered_samples_table3, fs_target_order3, fs_sample_order3, 
            title=dn + ' median coverage, filtered samples and all targets')

    # all samples and filtered targets
    ft_fn = '_'.join(['median_cov', dn, 'filt_targets.html'])
    filtered_targets_table2, ft_sample_order2, ft_target_order2 =\
            sort_target_table(filtered_targets_table, ft_sample_order, ft_target_order, by_row=True)
    filtered_targets_table3, ft_sample_order3, ft_target_order3 =\
            sort_target_table(filtered_targets_table2, ft_sample_order2, ft_target_order2, by_row=False)
    write_HTML_heatmap(ft_fn, filtered_targets_table3, ft_target_order3, ft_sample_order3, 
            title=dn + ' median coverage, all samples and filtered targets')


if __name__ == '__main__':
    main()
