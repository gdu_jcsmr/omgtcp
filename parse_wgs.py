#!/usr/bin/env python3

import sys

def return_metrics(fn):
    print(fn, file=sys.stderr)
    with open(fn, 'r') as f:
        while (True):
            line = next(f)
            if line.startswith('## METRICS'):
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                # collect header names
                #print(cols)
                mean_cov = cols[1]
                sd_cov = cols[2]
                median_cov = cols[3]
                pct_5x = cols[13]
                pct_10x = cols[14]
                pct_15x = cols[15]
                pct_20x = cols[16]
                pct_30x = cols[18]
                het_snp_sens = cols[26]
                het_snp_q = cols[27]
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                # collect values
                #print(cols)
                d = {'sample':fn.split('.')[0],
                     mean_cov:"{0:.1f}".format(float(cols[1])),
                     sd_cov:"{0:.1f}".format(float(cols[2])),
                     median_cov:"{0:.1f}".format(float(cols[3])),
                     pct_5x:"{0:.1f}".format(float(cols[13])*100),
                     pct_10x:"{0:.1f}".format(float(cols[14])*100),
                     pct_15x:"{0:.1f}".format(float(cols[15])*100),                   
                     pct_20x:"{0:.1f}".format(float(cols[16])*100),
                     pct_30x:"{0:.1f}".format(float(cols[18])*100),
                     het_snp_sens:"{0:.1f}".format(float(cols[26])*100),
                     het_snp_q:int(cols[27])}
                return d
    return {'sample':fn.split('.')[0]}


def main():
    print(return_metrics(sys.argv[1]))


if __name__ == '__main__':
    main()
