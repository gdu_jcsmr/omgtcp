#!/usr/bin/env python3

"""
Use CKAN API to create a remote package and upload files.

Bob Buckley, ABC JCSMR, Australian National Univerity 21/11/2017

This is initial test code.
"""

import os
# import time
import subprocess
import ckanapi
import sys
import glob


def main():
    # set up a our CKAN remote API access
    base = 'https://data.bioplatforms.com'
    apikey = sys.argv[1]
    remote = ckanapi.RemoteCKAN(base, apikey=apikey)

    # we set a high limit to avoid having to use paging
    limit = 50000

    sampdir = os.getcwd()
    sampno  = sampdir.rsplit(os.path.sep,1)[-1]
    gv = sampno+'*_R[12].fastq*'

    fnsa = glob.glob(os.path.join('trim', gv))
    assert len(fnsa)==2
    fnsb = ['results-qc.tgz']
    missing = [fn for fn in fnsb if not os.path.isfile(fn)]
    if missing:
        print('missing:', missing)
    assert all(os.path.isfile(fn) for fn in fnsb)
    assert len(fnsb)==1
    fns = fnsa + fnsb

    descs = (
                "Trimmed, merged and deduped R1 reads data file.",
                "Trimmed, merged and deduped R2 reads data file.",
                "fastqc output files in a tar.gz archive.",
            )

    if not fns:
        print("No files to upload.")
        return

    pkgname = 'abc-omg-%s' % (sampno)  # name for the package
    packages = remote.action.package_search(q='name:'+pkgname, include_private=True, rows=limit)['results']
    assert len(packages)<2
    if len(packages)==1:
        package = packages[0]
        print('found package', package['name'])
    else:
        # create the package
        package = remote.action.package_create(
            name  = pkgname,
            title = sampno+' processed data',  # descriptive (user-visible) title
            owner_org='anu-abc-upload',
            private=True  # require login to access
        )
        print("package created, browse contents at: https://data.bioplatforms.com/dataset/%s" % package['name'])
    reslist = [r['name'] for r in package['resources']]

    if False:
        print()
        for k, v in package.items():
            if k not in ['resources', 'organization']:
                print('', k, v, sep='\t')
        print('\torganization ...')
        for k, v in package['organization'].items():
            print('\t', k, v, sep='\t')
        for n, r in enumerate(package['resources']):
            print('', 'resources[%d]["name"]'%n, r['name'], sep='\t')

    # upload files
    for fn, desc in zip(fns, descs):
        fnx = fn.rsplit(os.path.sep, 1)[-1]
        if fnx in reslist: # already uploaded? then just skip
            continue
        print('uploading', fnx)

        with open(fn, 'rb') as fd:
            resource = remote.action.resource_create(
                package_id=package['id'],  # the ID of the package to add this file to
                name=fnx,  # descriptive title
                description=desc,
                upload=fd)
    print()
    print(sys.argv[0], 'done!')
    return

if __name__ == '__main__':
    main()
