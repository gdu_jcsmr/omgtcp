src=proteinlist.txt
tgt=MarsupialTargetsProt.fa

set -e

sort $src > tmp.txt

# use a one-line FAX format for easy processing
python3 ../code/fa2fax.py Sarcophilus_harrisii.DEVIL7.0.74.pep.all.fa | \
    sort -t '	' | join -t '	' tmp.txt - | \
    python3 ../code/fax2fa.py >$tgt
rm tmp.txt

len1=$( wc -l < $src)
len2=$( grep '^>' $tgt | wc -l)
[ $len1 == $len2 ] || {
    cat <<MSG
This process failed! The number of sequences does not match the number of listed protein targets.
No. targets = $len1
No. found   = $len2
MSG
    exit 1
}

mv $tgt ..

