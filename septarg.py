#!/usr/bin/env python3
"""
Created on 3 Jun 12:30:49 2017

@author: Bob Buckley
ANU Bioinformatics Consultancy (ABC), John Curtins School of Medical Research,
The Australian National University (ANU)

This script separates read files based on blastx/diamond output

There are paired end read files, say X_R1.fastq.gz and X_R2.fastq.gz
it is assumed that the equivalent of this command was run:
    $ samp=X
    $ for fn in $(cd srcdir; ls ${samp}*.fastq.gz)
    > do
    >    ./diamond -in srcdir/$fn -out tmp-xxx-$samp/${fn%.fastq.gz}.dout
    > done
    
Then run:
    $ mkdir destdir
    $ python3 septarg3.py ${samp}_R1.fastq.gz ${samp}_R1.dout destdir 
    
It will create a bunch or files destdir/<target>.fasta

Files x_R[12].fastq.gz and x_R[12].dout are transformed into lots of separate files
with reads associated with each protein target. x is the sample name.

This needs quite a bit of memory - but it is relatively fast (compared to my first version).
"""

#import sys
import os
import gzip
import argparse
import itertools as itx
import collections as cx

drecs = 'qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore'
TargetRecord = cx.namedtuple('TargetRecord', drecs.split())
drecs = 'rid targets'
TargetGroup = cx.namedtuple('TargetGroup', drecs.split())
qrecs = 'rid comments seq qual'
FastqRec = cx.namedtuple('FastqRec', qrecs.split())

class Fastq:
    def __init__(self, fn):
        self.filename = fn
        self.count = 0
        return
        
    def reader(self):
        "reader for FASTQ files"
        fn = self.filename
        myopen = lambda x: gzip.open(x, "rt") if fn.endswith('.gz') else open
        with myopen(fn) as src:
            count = 0
            for r in zip(src, src, src, src):
                hr = r[0].rstrip()[1:].split(None, 1)
                seq, qual = [r[i].rstrip() for i in (1,3)]
                count += 1
                yield(FastqRec(hr[0], hr[1] if len(hr)>1 else None, seq, qual))
        self.count = count
        return
    
def main():
    """
    Three phase process
    1. read Diamond/Blastx match file for read sequence IDs/target pairs
    2. store matching reads
    3. output per target file of interleaved paired ends
    """
    
    parser = argparse.ArgumentParser(description="separate reads that diamond/blast says match target sequences")
    parser.add_argument("rf1", help="file 1 of paired end reads")
    parser.add_argument("blastfile", help="blast/diamond output file")
    parser.add_argument("dstdir", help="destination directory for separated reads files")
    parser.add_argument("--stats", "-s", action='store_true', help="only output statistics, no separated reads files")
    parser.add_argument("--cutoff", "-c", type=int, default=0, help="only output targets with >N reads (default 0)")
    args = parser.parse_args()

    
    rfile1, dfile1, dstdir = args.rf1, args.blastfile, args.dstdir
    dfile2 = dfile1.replace('_R1', '_R2')
    
    print()
    print("reading Diamond/Blastx output files:")
    print('   ', dfile1)
    print('   ', dfile2)
    print()
         
    # Phase 1: read the paired diamond output files - collect relevant read IDs for each target
    with open(dfile1) as dsrc1, open(dfile2) as dsrc2:
        rseq = (TargetRecord(*r.rstrip().split("\t")) for f in (dsrc1, dsrc2) for r in f)
        gbseq = itx.groupby(sorted((r.sseqid, r.qseqid) for r in rseq), lambda x:x[0])
        # file only contains good matches
        trs = dict((t, trs) for t, rs in gbseq for trs in [tuple(r[1] for r in rs)] if len(trs)>args.cutoff)
    
    # make a set of the matching read IDs - from the diamond/blast match file
    print("Building read ID set.")
    sidset = frozenset(v for vs in trs.values() for v in vs)
    print("Number of reads to use in target assemblies:", len(sidset))
    # read all relevant reads (IDs in the set) from the paired end files
    
    #Phase 2
    print("collecting relevant sequences from:")
    rfile2 = rfile1.replace("_R1", "_R2")
    for fn in (rfile1, rfile2):
        print('   ', fn)
    print()
    
    # get the reads from the paired end read files
    rf1, rf2 = Fastq(rfile1), Fastq(rfile2)
    # needs itx.zip_longest (instead of zip()) for all counts to work
    prs = itx.zip_longest(rf1.reader(), rf2.reader())
    if args.stats:
        for x in prs:
            pass
    else:
        rdict = dict((r1.rid, ((r1.seq, r1.qual), (r2.seq, r2.qual))) for r1, r2 in prs if r1.rid in sidset)
        assert len(rdict)==len(sidset) # number of reads from the two sources should match
    assert rf1.count
    assert rf2.count
    assert rf1.count==rf2.count, "mismatched reads in paired-end reads files"
    
    
    print("No. or reads in sample data =", rf1.count)
    print("Blast/diamond matches %.2f%% of sample reads to targets." % (len(sidset)*100.0/rf1.count))
    if args.stats:
        return
    # Phase 3: output phase
    print("writing", len(trs), "assembly files.")
    for tn, rs in trs.items():
        mcnt = 0
        with open(os.path.join(dstdir, tn+'.fastq'), "w") as dst:
            for rid in rs:
                if rid in rdict:
                    for p, (s, q) in zip('12', rdict[rid]):
                        print('@'+rid+' '+p, file=dst)
                        print(s, file=dst)
                        print('+', file=dst)
                        print(q, file=dst)
                else:
                    mcnt += 1
        if mcnt:
            print("Warning! Target", tn, "missing", mcnt, "matching reads.")
    
    return
                
if __name__=="__main__":
    main()
    print("Done.")
