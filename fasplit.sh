#!/bin/bash
# code to split Target FASTA file into separate files for use with exonerate
dir=$1
fasta=$2
[[ $fasta == /* ]] || fasta=$PWD/$2
[ -d $dir ] && rm -r $dir
mkdir $dir
cd $dir
awk -F' ' '/^>/ {close(OUT); OUT=substr($1,2) ".fa"}; OUT {print >OUT}' $fasta
