#!/bin/bash

### run-sample-qual.sh
# Launches the per-sample quality assessment pipeline used for comparing
# all samples: pipe-sample-qual.sh
# This is meant to be run for all samples in a set, before running the main pipeline
###

# use /bin/sh syntax for die() function!
die () { date ; echo "$@" ; exit 1 ; }

topdir=${WORKDIR:-/mnt/OMG}
refname=${2:-marsupial}
codedir=$HOME/omg/code

source $codedir/param.sh # this file is installed by ansible
set -x	# after source so that OMG access code isn't recorded in the log

[ -n "$1" ] || die usage $0 sampleID
if [ $1 = . ] ; then
   s=${PWD##*/}
   cd ..
   [[ $PWD = $topdir ]] || die Run in a sub-directory of $topdir ... 
else
   cd $topdir
   s=$1	# should check for a valid sampleID or sampleID-datasetID - no it might be user supplied
   [ -d $s ] || mkdir $s || die "cannot create directory $PWD/$s"
fi
cd $s

if [[ $PATH != *$codedir:* ]] ; then
    export PATH=$codedir:$PATH
fi
if [[ $PATH != *$progdir:* ]] ; then
    export PATH=$progdir:$PATH
fi
if [[ $PATH != */usr/local/bin:* ]] ; then
    export PATH=/usr/local/bin:$PATH
fi

echo PATH=$PATH
type samtools || die samtools not found!

logfile=batch.log-$(date +%Y%m%d-%H.%M)

{
  echo Sample $s
  echo "Node IP addr: $ip"
  echo
  date
  set -ex
  # setup the Target Protein Reference if it isn't already in place
  [ -d protref ] || $codedir/protref_setup.sh $refname $s
  date
  [ -d raw ] || $codedir/omg-fetch.py $omgcode || die "omg-fetch failed"
  date
  $codedir/pipe-sample-qual.sh $s || 
    die "$s: per-sample quality pipeline failed!"
  date
  echo
  echo Done!
} >$logfile 2>&1
echo "See attached log." | mail -A $logfile -s "OMG on $ip: Sample $s done." $email
# should send an error subject if we failed
