# -*- coding: utf-8 -*-
"""
Created on Thu May 25 10:38:35 2017

@author: bobbuckley

Program to show how the contigs in an assembly match
need the contig file from spades assembly
and the diamond blastx output that compares the contigs to the target protein sequences 
"""
import fasta
import sys
import collections as nt

rnamap = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
    "UCU":"S", "UCC":"s", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":".", "UAG":".",
    "UGU":"C", "UGC":"C", "UGA":".", "UGG":"W",
    "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
    "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
    "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
    "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
    "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
    "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
    "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
    "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G",}
dnamap = dict((k.replace('U', 'T'), v) for k,v in rnamap.items())

def topseq(seq, off=0):
    return ''.join(dnamap[seq[i:i+3]] for i in range(off, len(seq)-2, 3))
    
def dreadr(fn):
    "read diamond -f 6 output records into Drec structure"
    
    drecs = 'qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore'.split()
    Drec = nt.namedtuple('Drec', drecs)
    
    funcs = [None, None, float, int, int, int, int, int, int, int, float, float]
    def dorow(r):
        return Drec(f(x) if f else x for f, x in zip(r.rstrip().split('\t'), funcs))
        
    with open(fn) as src:
        res = tuple(dorow(x) for x in src)
    return res
    
    
def main():
    fncontig, fndout, fnref = sys.argv[1:4]
    slen = 100
    
    drecs = 'qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore'.split()
    Drec = nt.namedtuple('Drec', drecs)
    
    tid = fncontig.split('/')[0][:-2]
    print("tid =", tid)

    # read the target sequence from the reference file    
    xpseq = None
    for hx, s in fasta.fasta_reader(fnref):
        h = hx.split(None,1)[0]
        if h==tid:
            xpseq = s
            break
    
    assert xpseq, "Protein sequence not in reference file." 
        
    revtab = str.maketrans("ACGT", "TGCA")
    
    with open(fndout) as src:
        nd = dict((x.qseqid, x) for x in (Drec(*x.rstrip().split('\t')) for x in src) if x.sseqid==tid)
 
    offset = min(int(x.sstart) for x in nd.values())-1
    print(xpseq[offset:offset+slen],'*')
    for h, s in fasta.fasta_reader(fncontig):
        if h not in nd:
            continue
        ndh = nd[h]
        pstart, pend = int(ndh.sstart)-1, int(ndh.send)-1
        qstart, qend = int(ndh.qstart)-1, int(ndh.qend)-1
        
        # reversal should be done properly
        # and the starts should align properly
        if qend<qstart:
            sx = s[qend:qstart+1][::-1].translate(revtab)
        else:
            sx = s[qstart:qend+1]
        assert len(sx)%3==0, "query match [%d:%d] %d=%d is not multiple of 3"%(qstart,qend, abs(qend-qstart), len(sx))


#        print(sx[:60])
        spaces = pstart-offset
        if spaces>=slen:
            continue
        outseq = ''.join(dnamap[sx[i:i+3]] for i in range(0, len(sx)-2, 3))
        outseq = ' '*spaces + outseq[:slen-spaces]
        print(outseq)
        
    return
    
if __name__ == "__main__":
    if len(sys.argv)!=4:
        print("usage: configs dout-file targetfile")
        sys.exit(1)
    main()
            

