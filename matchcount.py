#!/usr/bin/env python3
"""
Created on Wed 20 June 2018

@author: Bob Buckley, ANU Bioinformatics Consultancy, John Curtin SChool of Medical Research, ANU

Read a SAM file of scaffolds/contigs mapped onto all mitochondia
Count the number of best matches for each mitochondrial sequence ID
"""

import sys
import collections as cx

def main(samsrc):
    cutoff = 1.0 # as a percentage
    seqnms = cx.Counter()
    rcnt = 0
    for r in samsrc:
        if r.startswith('@'):
            continue
        flds = r.split('\t',3)
        if not int(flds[1])&2048:
            continue
        rcnt += 1
        if flds[2]!='*':
            seqnms[flds[2]] += 1
    cnt = sum(seqnms.values())
    if cnt<1:
        print("No matching sequences in SAM file.", file=sys.stderr)
        sys.exit(1)
    for v, n in sorted(((x, i) for i, x in seqnms.items()), reverse=True):
        if v*100<rcnt/cutoff: # better than 1%
            break
        print(n, v, "%.2f%%"%(v*100/rcnt), "%.2f%%"%(v*100/cnt), sep="\t")
    print("Total", rcnt, "records,", cnt, "matches (%.2f%%)"%(cnt/rcnt*100))
    return
    
if __name__=="__main__":
    if len(sys.argv)==2:
        with open(sys.argv[1]) as src:
            main(src)
    elif len(sys.argv)==1:
        main(sys.stdin)
    else:
        print("usage:", sys.argv[0], "[SAMfile]")
        sys.exit(1)