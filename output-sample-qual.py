#!/usr/bin/env python3

import sys
import os
import argparse
import json

# all_data_columns is shared by other qual scripts - protein mapping left out
#all_data_columns = [('Mapped reads r1 Prot', 'mapped_reads_r1'),
#            ('Mapped reads r2 Prot', 'mapped_reads_r2'),
all_data_columns = [('Mapped reads(DNA)', 'Mapped reads(DNA)'),
            ('Unmapped reads(DNA)', 'Unmapped reads(DNA)'),
            ('Mapped/unmapped ratio', 'Mapped/unmapped ratio'),
            ('Possible mismapped(DNA)','Possible mismapped(DNA)'),
            ('Mean cov', 'MEAN_COVERAGE'),
            ('Std Dev cov', 'SD_COVERAGE'),
            ('Median cov', 'MEDIAN_COVERAGE'),
            ('Pcnt of bases covered 5x', 'PCT_5X'),
            ('Pcnt of bases covered 10x', 'PCT_10X'),
            ('Pcnt of bases covered 15x', 'PCT_15X'),
            ('Pcnt of bases covered 20x', 'PCT_20X'),
            ('Pcnt of bases covered 30x', 'PCT_30X'),
            ('Het SNP sensitivity (Phred)', 'HET_SNP_Q'),
            ('Het SNP sensitivity (proportion)', 'HET_SNP_SENSITIVITY'),
            ('Estimated library size', 'ESTIMATED_LIBRARY_SIZE'),
            ('Pcnt duplication', 'PERCENT_DUPLICATION'),
            ('Pcnt inserts <130 bp', 'Fragments <130 bp'),
            ('Pcnt inserts 130-190 bp', 'Fragments 130-190 bp'),
            ('Pcnt inserts 191-399 bp', 'Fragments 191-399 bp'),
            ('Pcnt inserts 400+ bp', 'Fragments 400+ bp')]


def fragment_metrics(fn):
    """
    Read histogram of fragment sizes and return percentage
    of reads 130 bases or less, 130->190 bases, 191 bases
    to 399 bases, 400 bases +. 140->190 is perfect for
    merging, 400+ is crazy long. Picard uses the term "insert"
    """
    short = 0
    mergeable = 0
    long = 0
    crazy_long = 0

    with open(fn, 'r') as f:
        header = True
        for line in f:
            if line.startswith('insert'):
               header = False
            else:
               if header == True:
                   continue
               # line[0] is fragment length, line[1] is frequency
               cols = line.strip().split('\t')
               try:
                   frag, freq = map(int, cols)
               except:
                   continue  # malformed or empty line
               if frag < 130:
                   short += freq
               elif frag < 191:
                   mergeable += freq
               elif frag < 400:
                   long += freq
               else:
                   crazy_long += freq

    total = short + mergeable + long + crazy_long
    d = {'Fragments <130 bp':"{0:.2f}".format((short/total)*100),
         'Fragments 130-190 bp':"{0:.2f}".format((mergeable/total)*100),
         'Fragments 191-399 bp':"{0:.2f}".format((long/total)*100),
         'Fragments 400+ bp':"{0:.2f}".format((crazy_long/total)*100)}
    return d


def library_metrics(fn):
    """
    Parse the result of Picard library metrics
    """
    with open(fn, 'r') as f:
        while (True):
            try:
                line = next(f)
            except StopIteration:
                break
            if line.startswith('## METRICS'):
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                perc_dup_hdr = cols[8]
                est_lib_size_hdr = cols[9]
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                d = {perc_dup_hdr:"{0:.2f}".format(float(cols[8])*100),
                     est_lib_size_hdr:int(cols[9])}
                return d


def target_metrics(fn, read_length=100):
    """
    Read targets file and returns the sum of mapped reads as well as each target's coverage
    """
    mapped_reads = 0
    unmapped_reads = 0
    possible_mismapped = 0
    target_cov = {}
    with open(fn, 'r') as f:
        for line in f:
            cols = line.strip().split('\t')
            if line.startswith('*'):
                unmapped_reads = int(cols[2])
                continue
            cols = line.strip().split('\t')
            target = cols[0]
            reads = int(cols[2])
            cov = reads*read_length / int(cols[1]) # reads in bp / target length
            target_cov[target] = cov
            mapped_reads += int(cols[2])
            possible_mismapped += int(cols[3])
    if unmapped_reads == 0:
        ratio_reads = '+INF'
    else:
        ratio_reads = mapped_reads/unmapped_reads

    d = {'Mapped reads(DNA)':mapped_reads//2,
         'Unmapped reads(DNA)':unmapped_reads//2,
         'Mapped/unmapped ratio':ratio_reads,
         'Possible mismapped(DNA)':possible_mismapped//2,
         'targets':target_cov}
    return d


def wgs_metrics(fn):
    """ parse the result of Picard WGS metrics """
    with open(fn, 'r') as f:
        while (True):
            line = next(f)
            if line.startswith('## METRICS'):
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                # collect header names
                mean_cov = cols[1]
                sd_cov = cols[2]
                median_cov = cols[3]
                pct_5x = cols[13]
                pct_10x = cols[14]
                pct_15x = cols[15]
                pct_20x = cols[16]
                pct_30x = cols[18]
                het_snp_sens = cols[26]
                het_snp_q = cols[27]
                try:
                    line = next(f)
                except StopIteration:
                    break
                cols = line.strip().split('\t')
                # collect values
                d = {mean_cov:"{0:.1f}".format(float(cols[1])),
                     sd_cov:"{0:.1f}".format(float(cols[2])),
                     median_cov:"{0:.1f}".format(float(cols[3])),
                     pct_5x:"{0:.1f}".format(float(cols[13])*100),
                     pct_10x:"{0:.1f}".format(float(cols[14])*100),
                     pct_15x:"{0:.1f}".format(float(cols[15])*100),
                     pct_20x:"{0:.1f}".format(float(cols[16])*100),
                     pct_30x:"{0:.1f}".format(float(cols[18])*100),
                     het_snp_sens:"{0:.1f}".format(float(cols[26])*100),
                     het_snp_q:int(cols[27])}
                return d


def main():
    """
        Summarise the sample-qual pipeline output files in the qc-working subdirectory for a particular sample.
        Output a dictionary to a JSON file in the current directory.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('qualdir', help='path to sample-qual directory, filled by pipe-sample-qual.sh')
    parser.add_argument('sample', help='The facility_sample_id or other sample identifier')
    args = parser.parse_args()
    sample = args.sample
    all_data = {sample:{}}

    # iterate over files
    files = os.listdir(args.qualdir)
    for f in sorted(files):
        fpath = os.path.join(args.qualdir, f)
        # read diamond files - dropping this, it's too slow and adds nothing
        #if f.endswith('_dmnd.txt'):
        #    # continue  # skip for now as takes time
        #    with open(fpath, 'rt') as fh:
        #        for i, l in enumerate(fh):
        #            pass
        #        if '_R1' in f:
        #            all_data[sample]['mapped_reads_r1'] = i
        #        else:
        #            all_data[sample]['mapped_reads_r2'] = i

        if f.endswith('_targets.txt'):
            # proxy for diamond mapping
            all_data[sample].update(target_metrics(fpath))

        # read library.txt files
        elif f.endswith('_library.txt'):
            all_data[sample].update(library_metrics(fpath))

        # read wgs.txt files
        elif f.endswith('_wgs.txt'):
            all_data[sample].update(wgs_metrics(fpath))

        # read insert sizes
        elif f.endswith('_insert.txt'):
            all_data[sample].update(fragment_metrics(fpath))

        else:
            continue

    fn = sample + '_qual.json'
    with open(fn, 'wt') as out:
        json.dump(all_data, out, indent=4)


if __name__ == '__main__':
    main()
