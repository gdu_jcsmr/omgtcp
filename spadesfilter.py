#!/usr/bin/env python3
"""
Created on Mon Feb  5 00:57:26 2018

@author: Bob Buckley ABC John Curtin School of Medical Research, Auystralian National University

Filter SPAdes Contig or Scaffol outputs
"""

import fasta
import argparse

def fqr2name(fn1):
    pos = fn1.rfind('_R1')
    assert pos>=0
    return fn1[:pos+2]+"2"+fn1[pos+3:]  
    
def main():

    p = argparse.ArgumentParser()
    p.add_argument("-c", "--cover", type=float, default=0.0, help="minimum coverage for the contig/scaffold")
    p.add_argument("-n", "--length", type=int, default=0, help="min. length of a contig/scaffold (default=4)")
    p.add_argument("fasta", help="scaffold/config file being filtered")
    arg = p.parse_args()

    for h, s in fasta.fasta_reader(arg.fasta):
        fld = h.split('_')
        if arg.cover and float(fld[5])<arg.cover:
            continue
        if arg.length and int(fld[3])<arg.length:
            continue
        print('>'+h)
        print(s)
        
    return

if __name__=='__main__':
    main()
