#!/usr/bin/env python3

import numpy as np
import argparse as ap
import sys
from collect_targets import write_HTML_barchart
from math import ceil


def binning_generic(sample_score, args, num_bars=41):
    """
        Create the bar structures that will be used when plotting a barchart/histogram.
        Input: sample_score is a dict[sample] = score
        Output: bins[bin] = [(sample,score)], range = max(score)/(num_bars-1)
        Each bar is part of a histogram, so the height is frequency, bar widths span fixed ranges.
        Also return low/high bins if requested through args 
    """
    scores = [sample_score[k] for k in sample_score]
    bin_range = ceil(max(scores))/(num_bars-1)
    bins = {}
    low_score_bin = []
    high_score_bin = []
    for sample in sample_score:
        score = sample_score[sample]
        if args.low_cutoff_value is not None and score < args.low_cutoff_value:
            low_score_bin.append((sample, score))
        if args.high_cutoff_value is not None and score > args.high_cutoff_value:
            high_score_bin.append((sample, score))      
        bin = floor(score/bin_range)
        if bin not in bins:
            bins[bin] = []
        bins[bin].append((sample, score))      
    return bins, range, low_score_bin, high_score_bin


def build_bars(bins, range, args, low_score_bin=None, high_score_bin=None):
    """
        Input: bins[bin] = [(sample, score)], range=size of each bin
        Optional special bins
        Output: x,y,text for bar plot:
        x = [bar name], y = [frequency], 
        text=[samples in this bin]
    """
    sample_set = set()
    for bin in bins:
        for sample,score in bins[bin]:
            sample_set.add(s)

    num_samples = len(sample_set) 

    x, y, text = [], [], []   
    if args.low_score_cutoff is not None:
        x.append(args.low_score_cutoff)
        y.append(len(low_score_bin))        
        text.append('"' + '<br>'.join([samp for samp,score in low_score_bin]) + '"')
    for i, bin in enumerate(sorted(bins)):
        x.append((i+1)*range)
        y.append(len(bins[bin]))
        text.append('"' + '<br>'.join([samp for samp,score in bins[bin]]) + '"')
    if args.high_score_cutoff is not None:
        x.append(args.high_score_cutoff)
        y.append(len(high_score_bin))
        text.append('"' + '<br>'.join([samp for samp,score in high_score_bin]) + '"')
    return x, y, text


def write_HTML_barchart(filename, args, x, y, text, xlabel, ylabel, title=''):
    """
        Construct HTML file with embedded JS and data for barchart
    """
    with open(filename, 'w') as out:
        out.write('<html>\n<head>')
        out.write('    <script src="https://cdn.plot.ly/plotly-1.2.0.min.js">')
        out.write('</script>\n</head>\n')
        out.write('<body>\n')
        out.write('    <h2>' + title + '</h2>\n')
        out.write('    <div id="barchart" style="width:1700px;height:1000px;"></div>')
        out.write('    <script>\n')
        out.write('        var data = [\n')
        out.write('            {\n')
        out.write('                x: [' + ','.join(map(str,x)) + '],\n')
        out.write('                y: [' + ','.join(map(str,y)) + '],\n')
        out.write('                text: [' + ',\n'.join(text) + '],\n')
        out.write('                xaxis: ' + '"' + str(xlabel) + '"' + ',\n')
        out.write('                yaxis: ' + '"' + str(ylabel) + '"' + ',\n')
        out.write('                type: "bar"\n')
        out.write('            }\n')
        out.write('        ];\n')
        out.write('        var layout = {\n')
        out.write('            title: "' + title + '"' + ',\n')
        out.write('            xaxis: {\n')
        out.write('                text: "' + str(xlabel) + '"' + '\n')
        out.write('            },\n')
        out.write('            yaxis: {\n')
        out.write('                text: "' + str(ylabel) + '"' + '\n')
        out.write('            }\n')
        out.write('        }\n')
        out.write('        Plotly.newPlot("barchart", data, layout);\n')
        out.write('    </script>\n')
        out.write('</body>\n</html>\n')


def plot_statistic(stat):
    """ plot non-normalised histogram for a specific statistic """
    bins, range = binning(samples_targets_cov)
    print (len(bins), range)
    x, y, text = build_bars(bins, range)
    filename = 'rawcov_histogram.html'
    xlabel = 'Target coverage'
    ylabel = 'Frequency observed'
    title = 'Raw coverage histogram'
    write_HTML_barchart(filename, args, x, y, text, xlabel, ylabel, title=title)


def print_stat_overview(csv_records, topN):
    """
        Summarise each stat with min, max, mean, median
        Print the topN consistently high/low performing samples
    """
    header = None
    col_vals = {}
    for i, row in enumerate(csv_records):
        if i == 0:
            header = row
            for n, col in enumerate(row):
                col_vals[n] = []
        for n, col in enumerate(row):
            if n == 0:  # sample name
                cols_vals[n] = append(col)
                continue
            try:
                col_vals[n].append(np.float32(col))
            except ValueError:
                pass

    col_vals = {k:np.array(col_vals[k]) for k in col_vals}

    for n,col in enumerate(header):
      
        if len(cols_vals[n]) == 0:
            # no data available for this stat
            continue
        mean_v = np.mean(col_vals[n])
        med_v = np.median(col_vals[n])
        std_v = np.std(cols_vals[n])
        min_v = np.min(cols_vals[n])
        max_v = np.max(cols_vals[n])
                

def main():
    """
        Default: summarise all stats by min, max, mean, median
        Choose a stat: plot a histogram of this stat, on mouse-over show the most common samples
        Various options are provided to clarify important info
    """
    print()
    print('summarise_stats.py: summarise target capture quality statistics')
    print('Default use: summarise all stats by min, max, mean, median.')
    print('Optional: Show top N consistently poor and good samples, default=10')
    print('Optional: choose a stat to plot histogram. '+\
          'On mouse-over show the most common samples in the given bin')
    print('Optional: set low-score and/or high-score cutoffs and report all samples that fail')
    print()

    stat_choices = [('Mapped reads r1 Prot', 'mapped_reads_r1', 'a'), 
            ('Mapped reads r2 Prot', 'mapped_reads_r2', 'b'), 
            ('Mapped reads(DNA)', 'Mapped reads(DNA)', 'c'), 
            ('Unmapped reads(DNA)', 'Unmapped reads(DNA)', 'd'), 
            ('Mapped/unmapped ratio', 'Mapped/unmapped ratio', 'e'), 
            ('Mean cov', 'MEAN_COVERAGE', 'f'), 
            ('Std Dev cov', 'SD_COVERAGE', 'g'),
            ('Median cov', 'MEDIAN_COVERAGE', 'h'), 
            ('Pcnt of bases covered 20x', 'PCT_20X', 'i'), 
            ('Pcnt of bases covered 30x', 'PCT_30X', 'j'), 
            ('Het SNP sensitivity (Phred)', 'HET_SNP_Q', 'k'), 
            ('Het SNP sensitivity (proportion)', 'HET_SNP_SENSITIVITY', 'l'),
            ('Estimated library size', 'ESTIMATED_LIBRARY_SIZE', 'm'), 
            ('Pcnt duplication', 'PERCENT_DUPLICATION', 'n'), 
            ('Pcnt inserts <130 bp', 'Fragments <130 bp', 'o'), 
            ('Pcnt inserts 130-190 bp', 'Fragments 130-190 bp', 'p'), 
            ('Pcnt inserts 191-399 bp', 'Fragments 191-399 bp', 'q'), 
            ('Pcnt inserts 400+ bp', 'Fragments 400+ bp', 'r')]
    choice_text = ', '.join([t[2]+': '+t[0] for t in stat_choices])
    choice_description = {t[2]:t[0] for t in stat_choices}
    choice_field = {t[2]:t[1] for t in stat_choices}

    parser = ap.ArgumentParser('Summarise statistics')
    parser.add_argument('csvfile', help='Path to a project stat CSV file '+\
            'created by collect_stats.py')
    parser.add_argument('--topN', type=int, default=10, help='Show the N '+\
            'samples that consistently score poorly or well across statistics. '+\
            'This is only relevant if no statistic is chosen. Default is 10')
    parser.add_argument('--statistic', choices=['a','b','c','d','e','f','g','h',
            'i','j','k','l','m','n','o','p','q','r'], help='Build a histogram of '+\
            'scores from a chosen statistic. If the --low_cutoff_value or '+\
            '--high_cutoff_value options are present, then it creates bars '+\
            'for these categories and prints out all the samples that fall into '+\
            'these groups. Choose a letter representing a statistic '+\
            'from one of the following options. ' + choice_text)
    parser.add_argument('--low_cutoff_value', type=float, help='If given, this '+\
            'will create a special category bar to collect samples that report '+\
            'less than the given value. These samples are also printed to the '+\
            'screen. Only has an effect is a statistic is chosen.')
    parser.add_argument('--high_cutoff_value', type=float, help='If given, this '+\
            'will create a special category bar to collect samples that report '+\
            'more than the given value. These samples are also printed to the '
            'screen. Only has an effect is a statistic is chosen.')
    parser.add_argument('--no_plot', action='store_true', help="Don't create a "+\
            'histogram plot if a statistic is chosen')
    args = parser.parse_args()

    with open(args.csvfile, 'r', newline='') as f:
        stats_reader = csv.Reader(f, delimiter='\t')

        if args.statistic:
            print('Generating histogram for statistic:', choice-description[args.statistic])
            plot_histogram(stats_reader, choice_field[args.statistic], args)
        else:
            print_stat_overview(stats_reader, args.topN)            


if __name__ == '__main__':
    main()
