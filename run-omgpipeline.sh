#!/bin/bash
# use /bin/sh syntax for die() function!
die () { date ; echo "$@" ; exit 1 ; }

codedir=$HOME/omg/code
source $codedir/param.sh # this file is installed by ansible
set -x	# after source so that OMG access code isn't recorded in the log

topdir=${WORKDIR:-/mnt/OMG}
refname=${2:-marsupial}

[ -n "$1" ] || die usage $0 sampleID
if [ $1 = . ] ; then
   s=${PWD##*/}
   cd ..
   [[ $PWD = $topdir ]] || die Run in a sub-directory of $topdir ... 
else
   cd $topdir
   s=$1	# should check for a valid sampleID or sampleID-datasetID
   [ -d $s ] || mkdir $s || die "cannot create directory $PWD/$s"
fi
cd $s

if [[ $PATH != *$codedir:* ]] ; then
    export PATH=$codedir:/usr/local/bin:$PATH
fi
echo PATH=$PATH
type samtools || die samtools not found!

logfile=batch.log-$(date +%Y%m%d-%H.%M)

{
  echo Sample $s
  echo "Node IP addr: $ip"
  echo
  date
  set -ex
  # setup the Target Protein Reference if it isn't already in place
  [ -d protref ] || bash $codedir/protref_setup.sh $refname $s
  date
  bash $codedir/run-qc.sh . || 
    die "$s: QC pipeline failed!"
  date
  $codedir/pipe-assembly.ini . || 
    die "$s: stage1 pipeline failed!"
  date
  $codedir/pipe-haplotypes.ini . || 
    die "$s: stage2 pipeline failed!"
  date
  $codedir/run-stage3.sh . ||
    die "$s: stage3 script failed!"
  date
  $codedir/pipe-mtdna.ini . || 
    die "$s: mtdna pipeline failed!"
  date
  echo
  echo Done!
} >$logfile 2>&1
echo "See attached log." | mail -A $logfile -s "OMG on $ip: Sample $s done." $email
