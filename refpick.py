# -*- coding: utf-8 -*-

"""
build a de novo reference from all the scaffold files
based on information from a diamond/blastx comparison
of scaffold sequences to the target genes.

This code is used in the Oz Mammals Genomes project.

Bob Buckley, ABC @ JCSMR, Australian National University 30/5/2017

Read the Diamond/Blastx output and see if the best match for a scaffold
is the target sequence that it supposedly relates to.
If so, rename the FASTA sequence using the target name and add it to the "reference"
(a FASTA files that collects all the suitable scaffolds)
"""

import sys
import fasta

def first(xs):
    for x in xs:
        return x

def main():
    """
    pick a reference sequences for each target
    These are in either the configs.fasta or scaffolds.fasta files.
    """
    
    fn = sys.argv[1].rsplit('/', 1)[1] # get first filename
    assert fn in ['contigs.dout', 'scaffolds.dout']
    assert all(x.endswith(fn) for x in sys.argv[1:]) # check filenames are consistent
    
    # open the reference file for output
    with open("dref.fasta", "w") as dst: # dref.fasta is reference based on Diamond processing
        # process all the *.dout files
        rcnt = 0
        for dfn in sys.argv[1:]:
            fnparts = dfn.rsplit('/', 2)
            if fnparts[-1]!=fn:
                print("Bad filename:", dfn)
                sys.exit(1)
            assert fnparts[-2].endswith('-d')   # this is how the pipeline works
            tgt = fnparts[-2][:-2] # there must be a containing directory
            # tgt = dfn[:-len("scaffolds.dout")-3]
            # print(dfn, tgt)
            with open(dfn) as dsrc:
                firstrec = first(dsrc) # assumes *.dout file have the longest and best contig/scaffold first - should check this!
                if firstrec is None:
                    # deal with empty file!
                    continue
                flds = firstrec.split("\t",2)
            if flds[1]!=tgt:
                continue    # drop if target is not the best match to the contig/scaffold

            scfn = dfn[:-4]+"fasta"     # construct FASTA filename

            # this could use first(fasta_reader()) - since first in *.dout means first in contig/scaffold file
            for hx, s in fasta.fasta_reader(scfn):
                rid = hx.split(None,1)[0]
                # maybe should check coverage > 30
                # or we could count and report the number with low coverage
                # could attach diamond/blastx info. for later use
                if rid==flds[0]: # this is always true
                    dst.write('>'+tgt+' '+hx+'\n')
                    dst.write(s+"\n")
                    rcnt += 1
                    break
    print("Reference has", rcnt, "sequences.")
    return
    
if __name__ == "__main__":
    main()