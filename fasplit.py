#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
split a multi-sequence FASTA file into separate files

@author: Bob Buckley ABC JCSMR 1/11/2018

This code is part of the OMG project
It relies on the comment part of the FASTA headers having
the right format (as produced by x8pick.py)
that is, the comment part of the header is in 3 parts:
    part 1 - an identifier
    part 2 - exon start position
    part 3 - exon end position
"""

version = "0.3"

import os
import re
import sys
import argparse
# import collections

import fasta

def main():
    ll = 72
    parser = argparse.ArgumentParser(description="Script to split a FASTA file into separate (one per file) FASTA sequences")
    xg = parser.add_mutually_exclusive_group()
    parser.add_argument("-v", "--version", action="store_true", help="exit after displaying version number")
    xg.add_argument("-x", "--exon", action="store_true", help="trim FASTA sequence to the exon as indicated in the OMG format header")
    xg.add_argument("-p", "--parts", action="store_true", help="like -x split FASTA sequence to the 5', exon and 3' segments")
    parser.add_argument("-c", "--comment", help="include comment in FASTA header of each record/file")
    parser.add_argument("dir", help="destination directory name for the separate FASTA files")
    parser.add_argument("fasta", help="name of the input FASTA file")
    args = parser.parse_args()

    global version
    if args.version:
        print(sys.argv[0].rsplit('/',1)[-1], '-', "Version", version, file=sys.stderr)
        return 0
    
    # find the input FASTA file
    fn = args.fasta
    rdr = fasta.fasta_reader(fn)
    
    # make the destination directory
    for ext in ('-3p', '-exon', '-5p') if args.parts else ('-exon',) if args.exon else ('',):
        os.makedirs(args.dir+ext, exist_ok=True)

    xext =''
    nfix = { '-5p':re.compile(r'N+$'), '-3p':re.compile(r'^N+'), '-exon':re.compile(r'^$') }
    for h, seq in rdr:
        fld = h.split()
        if len(fld)<4:
            print("FASTA =", args.fasta)
            print("h =", h)
            print("h.split() =", fld)
            continue
        if args.exon:
            arglist =(('', int(fld[2]), int(fld[3])),)
        elif args.parts:
            xext = '-'+ext
            arglist = ( ('-5p', None, int(fld[2])), ('-exon', int(fld[2]), int(fld[3])), ('-3p', int(fld[3]), None))
        else:
            arglist = (('', None, None),)
        hx = h
        for d, start, finish in arglist:
            if start==len(seq) or finish==0: # do not output empty sequences
                continue
            s=seq[start:finish] if any(x is not None for x in (start, finish)) else seq
            s = nfix[d].sub('', s) # delete appropriate leading/trailing Ns
            if not len(s):
                continue
            fnx = os.path.join(args.dir+d, fld[0]+'.fa')
            with open(fnx, 'wt') as dst:
                if args.comment:
                    # s ID is target and sample ID, with (5p/exon/3p)
                    hx = fld[0]+'-'+args.comment+d
                print('>'+hx, file=dst)
                for i in range(0, len(s), ll):
                    print(s[i: i+ll], file=dst)

    return
    
if __name__=="__main__":
    main()
