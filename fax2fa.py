
"""
Coonvert FAX file (single line FASTA file) to a FASTA file

Bob Buckley
23 May @017
ANU Bioinformatics Consultancy
John Curtin School of Medical Research
Australian National University
"""

import itertools as itx
import sys

def main(src):
    for r in src:
        flds = r.rstrip().split('\t')
        assert len(flds)>=2
        seq = flds[1]
        del flds[1:2]
        print('>'+' '.join(flds))
        print(seq)
    return

if __name__=="__main__":
    if len(sys.argv)>1:
        with open(sys.argv[1]) as src:
            main(src)
    else:
        main(sys.stdin)
