# -*- coding: utf-8 -*-

"""
Create a coloured grid using SVG

Bob Buckley, ABC @ JCSMR, Australian National University
14/6/2017
"""

import sys

def grid(dst, data, size=(100, 100), bg='rgb(248, 248, 248)', margin=None):
    cmin = min(x for r in data for x in r)
    cmax = max(x for r in data for x in r)
    cmin, cmax = 0, 100
    def colour(x):
        c = round((x-cmin)*255/(cmax-cmin))
        return "rgb(%d, %d, %d)"%(c//2, c//2, c)
    ymax=len(data)
    xmax = max(len(r) for r in data)
    xstep = size[0]/xmax
    ystep = size[1]/ymax
    step = int(min(xstep, ystep))
    if margin:
        vx = (xmax*step+2*margin[0], ymax*step+2*margin[1], bg)
    else:
        vx = (xmax*step, ymax*step, bg)
    print("<svg  width='%d' height='%d' style='background:%s' >"%vx, file=dst)
    mx = ''
    if margin:
        print("  <g transform='translate(%d %d)' >"%(margin[0], margin[1]), file=dst)
        mx = '  '
    for yidx, row in enumerate(data):
        for xidx, val in enumerate(row):
            vs = xidx*step, yidx*step, step, step, colour(val)
            print(mx+"  <rect x='%d' y='%d' width='%d' height='%d' style='fill: %s;' />"%vs, file=dst)
    if margin:
        print("  </g>", file=dst)
    print("</svg>", file=dst)
    return
    
def main(dst):
    print("<!DOCTYPE html>\n<html>\n<body>\n<h1>Figure</h1>", file=dst)
    mydata = (
                (70, 80, 30, 80, 75, 20, 20, 80),
                (80, 80, 70, 30, 80, 20, 40, 70),
                (60, 70, 70, 70, 30,  0,  0,  0),
                (70, 40, 80, 80, 75, 80, 50,  0),
                (80, 80, 80, 80, 70, 20, 80, 75),
             )
    grid(dst, mydata, size=(800, 500), margin=(20, 15))
    print("</body>\n</html>", file=dst)
    return
    
if __name__=="__main__":
    main(sys.stdout)