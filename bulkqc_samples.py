#!/usr/bin/env python3

import sys
import subprocess
import os
import parse_library
import parse_wgs
import parse_insert
import parse_targets
import argparse

def main():
    """
        Go through the output directory for the QC pipeline 
        and collect all statistics into a group of dictionaries
        and report this as JSON, embedded in an HTML file, 
        which also contains the necessary Javascript to
        display Highcharts plots of this data

        Make Excel CSV file
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('statdir', help="path to stats directory, filled by collect_targets2")
    args = parser.parse_args()
    all_data = {}
    all_data_columns = [('Mapped reads r1 Prot', 'mapped_reads_r1'), \
            ('Mapped reads r2 Prot', 'mapped_reads_r2'), \
            ('Mapped reads(DNA)', 'Mapped reads(DNA)'), \
            ('Unmapped reads(DNA)', 'Unmapped reads(DNA)'), \
            ('Mapped/unmapped ratio', 'Mapped/unmapped ratio'), \
            ('Mean cov', 'MEAN_COVERAGE'), \
            ('Std Dev cov', 'SD_COVERAGE'), \
            ('Median cov', 'MEDIAN_COVERAGE'), \
            ('Pcnt of bases covered 20x', 'PCT_20X'), \
            ('Pcnt of bases covered 30x', 'PCT_30X'), \
            ('Het SNP sensitivity (Phred)', 'HET_SNP_Q'), \
            ('Het SNP sensitivity (proportion)', 'HET_SNP_SENSITIVITY'),\
            ('Estimated library size', 'ESTIMATED_LIBRARY_SIZE'), \
            ('Pcnt duplication', 'PERCENT_DUPLICATION'), \
            ('Pcnt inserts <130 bp', 'Fragments <130 bp'), \
            ('Pcnt inserts 130-190 bp', 'Fragments 130-190 bp'), \
            ('Pcnt inserts 191-399 bp', 'Fragments 191-399 bp'), \
            ('Pcnt inserts 400+ bp', 'Fragments 400+ bp')]
    # iterate over files
    files = os.listdir(args.statdir)
    for f in sorted(files):
        fpath = os.path.join(args.statdir, f)
        print (fpath)
        # read diamond files
        if f.endswith('_dmnd.txt'):
            # continue  # skip for now as takes time
            p1 = subprocess.Popen(['cut', '-f', '1', fpath], stdout=subprocess.PIPE)
            p2 = subprocess.Popen(['sort'], stdin=p1.stdout, stdout=subprocess.PIPE)
            p3 = subprocess.Popen(['uniq'], stdin=p2.stdout, stdout=subprocess.PIPE)
            p4 = subprocess.Popen(['wc', '-l'], stdin=p3.stdout, stdout=subprocess.PIPE)
            result = p4.communicate()[0]
            sample = f.split('_R')[0]
            if sample not in all_data:
                all_data[sample] = {}
            if '_R1' in f:
                all_data[sample]['mapped_reads_r1'] = int(result)
            else:
                all_data[sample]['mapped_reads_r2'] = int(result)

        elif f.endswith('_targets.txt'):
            # proxy for diamond mapping
            #if 'mapped_reads_r1' not in all_data[sample]:
            sample = f.split('_targets.txt')[0]
            if sample not in all_data:
                all_data[sample] = {}
            d = parse_targets.return_metrics(fpath)
            all_data[sample].update(d)

        # read library.txt files
        elif f.endswith('_library.txt'):
            sample = f.split('_library.txt')[0]
            if sample not in all_data:
                all_data[sample] = {}
            d = parse_library.return_metrics(fpath)
            all_data[sample].update(d)

        # read wgs.txt files
        elif f.endswith('_wgs.txt'):
            sample = f.split('_wgs.txt')[0]
            if sample not in all_data:
                all_data[sample] = {}
            all_data[sample].update(parse_wgs.return_metrics(fpath))

        # read insert sizes
        elif f.endswith('_insert.txt'):
            sample = f.split('_insert.txt')[0]
            if sample not in all_data:
                all_data[sample] = {}
            all_data[sample].update(parse_insert.return_metrics(fpath))

        else:
            continue

    dn = args.statdir.strip().rstrip('/').split('/')[-1]
    out_fn = dn+'_all_data.csv'
    print('Writing to', out_fn)
    with open(out_fn, 'w') as out:
        samples = sorted(all_data)
        out.write('Sample' + '\t' + '\t'.join([head for head, col in all_data_columns]) + '\n')
        for sample in samples:
            line_data = [all_data[sample].get(col, "NA") for head,col in all_data_columns]
            sample_line = '\t'.join(map(str, line_data))
            out.write(sample + '\t' + sample_line + '\n')
                
    sys.exit(0)    
    # Median Coverage plots
    
    with open(dn+'_median_cov.csv', 'w') as out:
        for s in wgs_data:
            out.write(s['name'] + ',' + str(s['MEDIAN_COVERAGE'])+'\n')

    with open(dn+'_30x_cov.csv', 'w') as out:
        for s in wgs_data:
            out.write(s['name'] + ',' + str(s['PCT_30X'])+'\n')

    with open(dn+'_est_lib_size.csv', 'w') as out:
        for s in library_data:
            out.write(s['name'] + ',' + str(s['ESTIMATED_LIBRARY_SIZE']) + '\n')

    with open(dn+'_pct_dup.csv', 'w') as out:
        for s in library_data:
            out.write(s['name'] + ',' + str(s['PERCENT_DUPLICATION']) + '\n')

    with open(dn+'median_coverage.html', 'w') as out:
        with open('html_header.txt', 'r') as f:
            for line in f:
                out.write(line)
        with open('boxplot.txt', 'r') as f:
            for line in f:
                out.write(line)
        #insert data
        d_line = ','.join(map(str, data))
        out.write('data=[' + d_line + ']\n')
            
        with open('html_footer.txt', 'r') as f:
            for line in f:
                out.write(line)
    # 30xCov%
    # %duplication
    # estimated library size

if __name__ == '__main__':
    main()
