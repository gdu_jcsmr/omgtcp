#!/usr/bin/env python3

import sys
import os

def main():
    indirname = sys.argv[1]
    filenames = [f for f in os.listdir(indirname) if 'fastq' in f.lower() or 'fq' in f.lower()]
    unique_names = set()
    for f in filenames:
        fn_parts = f.split('_')
        unique_names.add('_'.join(fn_parts[:-3]))
    for f in sorted(unique_names):
        print(f)

if __name__ == '__main__':
    main()
