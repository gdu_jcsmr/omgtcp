#!/usr/bin/env python3
"""
Created on Fri Sep 28 13:15:01 2018

@author:  Bob Buckley, ABC JCSMR, ANU

Combine multiple muscle output files.
Create input for iqtree program

"""

import os
import sys
import glob
import argparse
import collections

import fasta

#SeqHdr = collections.namedtuple('SeqHdr', 'info start end match offset seq'.split())
SeqHdr = collections.namedtuple('SeqHdr', 'info seq'.split())
def mkSeq(h, s):
    fld = h.split()[0].split('-')
    return fld[0], SeqHdr(fld[1], s)

def muscle_reader(fn):
    return (mkSeq(h, s) for h, s in fasta.fasta_reader(fn))
    
def main():
    parser = argparse.ArgumentParser(description="combine muscle output into a single input file for iqtree")
    # parser.add_argument("-v", "--version", action="store_true", help="exit after displaying version number")
    parser.add_argument("muscdir", help="name of the directory containing the muscle output files")
    parser.add_argument("muscext", help="extension for muscle output files e.g. -mx.fa")
    arg = parser.parse_args()
    
    dx = {}
    tls = {}
    plen = 0 # final length of all sequences
    # input phase - read all the muscle output files
    globname = os.path.join(arg.muscdir, '*'+arg.muscext)
    # get all the muscle files
    for fn in sorted(glob.glob(globname)):
        for h, s in muscle_reader(fn):
            # s.info has sample ID ...
            if s.info not in dx:
                dx[s.info] = {}
            if h not in tls:
                tls[h] = len(s.seq)
            else:
                # check all sequences for the same target are the same length
                assert tls[h]==len(s.seq)
            dx[s.info][h] = s.seq
        
    # output phase
    print(len(dx), "samples", file=sys.stderr)
    linelen = 64
    ts = sorted(x for x in tls)
    print(len(tls), "targets", file=sys.stderr)
    plen = sum(tls.values())
    assert plen>0
    with open(arg.muscdir+arg.muscext, "wt") as dst:
        # output one long sequence for each sample
        # missing target sequences are filled with enough '-' characters
        for samp in dx:
            seqs = dx[samp]
# note: iqtree doesn't accept ! - replace and !s with -s
            seq = ''.join(seqs[t].replace('!', '-') if t in seqs else '-'*tls[t] for t in ts)
            print('>'+samp, file=dst)
            for i in range(0, len(seq), linelen):
                print(seq[i:i+linelen], file=dst)                
    print(plen, "bases per sample", file=sys.stderr)
    return
    
if __name__=="__main__":
    main()

