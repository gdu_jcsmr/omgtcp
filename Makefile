.SUFFIXES:
.SUFFIXES: .contig .fastq .fasta

progdir ::= ~/omg/prog
spadesdir ::= $(progdir)/SPAdes-3.12.0-Linux
spades ::= $(spadesdir)/bin/spades.py

kmer ::= -k 25,35,45,55,65,75
perc ::=

all:
	for f in tgtdir/*2232?.fastq ; do \
	echo Making $$f ; echo ;\
	  $(MAKE) -C tgtdir $$(basename $$f .fastq).fasta ; \
	done

%-c.fasta: %.fastq
	echo "\$$*=$*" "\$$@=$@" "\$$<=$<"
	$(progdir)/bbmap/bbmerge.sh in=$< out=$*m.fq outu=$*u.fq >/dev/null
	$(spades) $(kmer) -t 1 --merged $*m.fq --12 $*u.fq -o $*-t >/dev/null
	# mv $*-t/contigs.fasta $@
	rm $*m.fq $*u.fq
	exonerate $(perc) --model protein2genome --ryo 'ryo:\t%pi\t%tal\t%tab\t%tae\t%qab\t%qae\t%s\t%ti\n' ../protref/$*.fa $*-t/contigs.fasta --showvulgar no --showalignment no | grep '^ryo:' | sort -k 2nr,3n | ~/omg/code/x8pick.py $*-t/contigs.fasta $*
