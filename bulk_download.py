#!/usr/bin/env python3

import subprocess
import ckanapi
import sys
import os
import argparse
import csv

def main():
    base = 'https://data.bioplatforms.com'
    if 'OMG_APIKEY' not in os.environ:
        print('OMG_APIKEY environment variable must be set')
        sys.exit(1)
    apikey = os.environ['OMG_APIKEY']
    remote = ckanapi.RemoteCKAN(base, apikey=apikey)
        
    # we set a high limit to avoid having to use paging
    limit = 50000

    packages = remote.action.package_search(q='type:omg-exon-capture', 
            include_private=True, rows=limit)['results']
    print('Found', len(packages), 'samples.')

    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', help='Path to data set CSV')
    parser.add_argument('identifier', 
            help='Name for dataset, no spaces please')
    parser.add_argument('--use_subfolders', action='store_true',
            help='Create subfolders, as per projects on cloud '+\
            'nodes. Default will download all files to one folder')
    args = parser.parse_args()    

    facility_sample_ids = set()
    with open(args.dataset, 'r', newline='') as csvfile:
        r = csv.DictReader(csvfile)
        for row in r:
            facility_sample_ids.add(row['facility_sample_id'])

    samples = [p for p in packages if p['facility_sample_id'] in\
            facility_sample_ids]
    if len(samples) == 0:
        print('Samples not found')
        sys.exit(1)
    print(len(samples), 'samples found')

    for i, sample in enumerate(samples):
        if args.use_subfolders is not None:
            fdir = os.path.join(args.identifier, 
                    sample['facility_sample_id'], 'raw')
            if os.path.isdir(fdir):
                print('Destination directory already exists:', fdir)
                sys.exit(1)
            tdir = os.path.join(args.identifier, 
                    sample['facility_sample_id'], 'xraw')
            if os.path.isdir(tdir):
                print('Temporary directory already exists:', tdir)
                sys.exit(1)
            os.makedirs(tdir)

            wget_args = [
               'wget',
               '--no-http-keep-alive',
               '--header=Authorization: ' + apikey,
               '-c', '-q',
               '-t', '0',
               '-P', tdir,
            ]

        else:
            fdir = os.path.join(args.identifier)
            
            wget_args = [
               'wget',
               '--no-http-keep-alive',
               '--header=Authorization: ' + apikey,
               '-c', '-q',
               '-t', '0',
               '-P', fdir,
           ]

        for resource in sample['resources']:
            wget_cmd = wget_args + [resource['url']]
            print('fetching', wget_cmd[-1].rsplit('/', 1)[-1])
            # print('calling wget: %s' % (wget_args))
            res = subprocess.run(wget_cmd)
            if res.returncode: # bad is non-zero
                print(wget_cmd[0], 'returned', res.returncode)
                sys.exit(res.returncode)
        if args.use_subfolders:
            os.rename(tdir, fdir)

        print('Downloaded', sample['facility_sample_id'], 
                str(i+1)+'/'+str(len(facility_sample_ids))) 


if __name__ == '__main__':
    main()
