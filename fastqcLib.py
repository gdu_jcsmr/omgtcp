# -*- coding: utf-8 -*-

"""
Read fastqc output text file into a data structure.

Bob Buckley, ANU Bioinformatics Consultancy,
John Curtin School of Medical Research,
Australian National University
15/6/2017
"""
import os

class FastqcRec:
    def __init__(self, dirname):
        # read the summary.txt file
        with open(os.path.join(dirname, "summary.txt")) as src:
            self.summary = tuple(x.rstrip().split('\t', 2)[:2] for x in src)
        
        return
                