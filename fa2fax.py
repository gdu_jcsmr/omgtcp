
"""
Code to creat a single line version of FASTA so
UNIX tools can process tables.

Bob Buckley
23 May @017
ANU Bioinformatics Consultancy
John Curtin School of Medical Research
Australian National University
"""

import itertools as itx
import sys

def main(src):
    inp = itx.groupby(src, lambda x:x.startswith('>'))
    for t, g in inp: 
        if t:
            hx = list(g)
            continue
        seq, h = ''.join(s.strip() for s in g), hx[-1]
        hs = h[1:].split(None,1)
        flds = [hs[0], seq]+[x.strip() for x in hs[1:]]
        print('\t'.join(flds))
    return

if __name__=="__main__":
    if len(sys.argv)>1:
        with open(sys.argv[1]) as src:
            main(src)
    else:
        main(sys.stdin)
