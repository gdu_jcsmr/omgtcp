#!/bin/bash

die () { echo "$@" >&2 ; exit 1 ; }

codedir=$HOME/omg/code
progdir=$HOME/omg/prog
spadesdir=$progdir/SPAdes-3.12.0-Linux

[ -d $dpadesdir ] || die SPAdes is missing. No $spadesdir directory
[ -f $spadesdir/bin/spades.py ] || die SPAdes program not found

# assemble the separated reads for each protein target

[ ${1#*/} = $1 -a .${1#.} != $1 ] || die "Workdir must be a (visible) subdirectory."
[ -d protref ] || die "protref - separate target protein reference files are missing"
[ -s protref.fa ] || dir "target protein reference file is missing"
[ -s protref.dmnd ] || dir "target protein reference Diamond indexDiamond index fa file is missing"
set -e
cd $1
declare -i th=$2 i 

# the following section works with directories with over 1000 files
# this could cause performance issues ... it could be fixed
echo
echo Assemble reads for each target protein
# assemble contigs/scaffolds from each separate set of target-related reads
# the following does a large number of assemblies
# only keep the $using.fasta files
echo Working in $PWD
set +x
declare -a fns=($(for x in *.fastq; do echo ${x%.fastq}; done))
let notargets=${#fns[*]}
echo $notargets target files to process!
[ $notargets -eq 0 ] && die No files to process.
set -x
for (( i=0; $i<$th; i++ ))
do
  for ext in fasta
  do
    [ -f thread$i.$ext ] && rm thread$i.$ext
  done
  let lim=${#fns[*]}
  # let lim=20 # for testing
  for (( n=i; $n<$lim; n+=th ))
  do
    tbn=${fns[$n]}
    tfn=${tbn}.fastq
    [ $tbn-c.fasta -nt $tfn ] && continue # this target already done!
    [ -s $tbn-t/contigs.fasta ] || {
      echo >&2
      echo Assembling target "[$n]" $tfn >&2
      $HOME/omg/prog/bbmap/bbmerge.sh in=$tfn out=${tbn}m.fq outu=${tbn}u.fq >/dev/null
      # use one CPU core in parallel SPAdes runs - uses CPUs better
      #kmer="--careful -k 27,33,39"
      kmer="-k 25,35,45,55,65,75"
      $spadesdir/bin/spades.py $kmer -t 1 --merged ${tbn}m.fq --12 ${tbn}u.fq -o $tbn-t >/dev/null || {
        echo Spades finished abnormally for $tbn. >&2
        touch $tbn-c.fasta
        rm -r $tbn-t/* ${tbn}[mu].fq
        continue
      }
      # remove the merged files used for assembly
      rm ${tbn}[mu].fq

      # tidy up the assembly directory - just keep the contig file!
      [ -s $tbn-t/contigs.fasta ] && mv $tbn-t/contigs.fasta cx$i.fasta
      touch $tbn-t/tmp
      rm -r $tbn-t/*
      [ -s cx$i.fasta ] && mv cx$i.fasta $tbn-t/contigs.fasta
    }
    [ $tbn-c.fasta -nt $tbn-t/contigs.fasta ] || {
        #perc="--percent 70" #flag doesn't seem to work, x8pick does the job
        perc=
        exonerate $perc --model protein2genome --ryo 'ryo:\t%pi\t%tal\t%tab\t%tae\t%qab\t%qae\t%s\t%ti\n' ../protref/$tbn.fa $tbn-t/contigs.fasta --showvulgar no --showalignment no | grep '^ryo:' | sort -k 2nr,3n | $HOME/omg/code/x8pick.py $tbn-t/contigs.fasta $tbn
    }
    ## [ -d $tbn-t ] && rm -r $tbn-t
#    [ -s $tbn-c.fasta ] || continue
#    [ $tbn-c.txt -nt $tbn-c.fasta -o ! -s $tbn-c.fasta ] || {
#          exonerate --model protein2genome ../protref/$tbn.fa $tbn-c.fasta >$tbn-c.txt
#    }
  done 2>step2-1-$i.log  &
done
wait # for the threads to finish
ls -l step*.log

cat step2-1-*.log && rm step2-1-*.log

declare -i contigcount=$(ls *-c.fasta | wc -l)
(( $contigcount<$notargets/10 )) && die Just $contigcount contigs found for $notargets targets

echo assembly stage done.
