#!/usr/bin/env python3
"""
Created on Fri Feb  2 00:08:05 2018

@author: Bob Buckley ABC John Curtin School of Medical Research, Auystralian National University

Filter reads.
File 1 is a list of read identifiers.
File 2 is a SAM files
File 3 is the name of the R1 FASTQ output file (it will create an R2 file as well).
"""

import sys
def main():
    idfile = sys.argv[1]
    dfn1 = sys.argv[3]
    pos = dfn1.rfind('_R1')
    dfn2 = dfn1[:pos+2]+"2"+dfn1[pos+3:]
    # read the list of required read identifiers
    with open(idfile) as src:
        idset = frozenset(x.strip() for x in src)
    # read a SAM file and output filtered R1 and R2 FASTQ files
    with open(sys.argv[2]) as src, open(dfn1, "wt") as dst1, open(dfn2, "wt") as dst2:
        for r1, r2 in zip(src, src): # paired reads are adjacent
            rid = r1.split('\t', 1)[0]
            if rid in idset:
                assert r2.startswith(rid) # check Read IDs match
                for r, dst in ((r1, dst1), (r2, dst2)):
                    flds = r.split('\t')
                    print('@'+flds[0], file=dst)
                    print(flds[9], file=dst) # sequence
                    print('+', file=dst)
                    print(flds[10], file=dst) # quality
    return

if __name__=='__main__':
    main()
