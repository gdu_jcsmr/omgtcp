#!/bin/bash

# script to get GFF files for haplotype files
set -e

codedir=$HOME/omg/code
datadir=protref

[ -z "$1" ] && { echo usage: $0 sampdir ; exit 1 ; }
set -x
[ $1 != . ] && cd $1
for f in *.[12D].fa
do
  gff=${f%.fa}.gff
  [ $gff -nt $f ] && continue
  [ -d dx ] && rm -r dx ; mkdir dx
  # split haplotype sequences into separate files in workdir dx
  $codedir/fasplit.sh dx $f
  cd dx
  for seq in *.fa
  do
    exonerate -m protein2genome --showvulgar false --showalignment false --showtargetgff true -q ../$datadir/$seq -t $seq
  done | grep -v -E '^(#|--|Command line:|Hostname:)' >$gff
  # move completed GFF file to final destination
  mv $gff .. && echo Created $gff
  # clean up temporary directory
  cd ..
  rm -r dx
done
