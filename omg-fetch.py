#!/usr/bin/env python3
"""
Fetch OMG exon datasets for processing

Bob Buckley & Cameron Jack,
ANU Bioinformatics Consultancy,
John Curtin School of Medical Research
Australian National University
Acton ACT 0200

10/11/2017
v2 2018 July
This works in a directory with the 5-digit exon dataset name and an optional
dash then dataset ID
Works on facility_sample_id (sampleID_libraryID_barcode)
e.g. 53348_AHGVYGBCX2_ACCAACT
"""

import subprocess
import ckanapi
import sys
import os

def main():
    # should cd to /work/OMG first!!!! ???
    if len(sys.argv)!=2:
        print('usage:cd facility_sample_id ;', sys.argv[0], 'apikey')
        sys.exit(1)

    # set up a our CKAN remote API access
    base = 'https://data.bioplatforms.com'
    apikey = sys.argv[1]
    facility_sample_id = os.getcwd().rsplit(os.path.sep, 1)[-1]
    remote = ckanapi.RemoteCKAN(base, apikey=apikey)

    # we set a high limit to avoid having to use paging
    limit = 50000

    packages = remote.action.package_search(q='type:omg-exon-capture', include_private=True, rows=limit)['results']
    print('found', len(packages), 'samples.')
    # to download a file, we simply have to send our API key in the Authorization header
    # you can use Python, but I recommend `wget` with the arguments provided as it'll
    # handle download resumption, etc: required for the extremely large files we have
    # for OMG (tens of gigabytes per file)
    samples = [p for p in packages if p['facility_sample_id'] == facility_sample_id]
    if len(samples) == 0:
        print(facility_sample_id, 'not found')
        sys.exit(1)
    if len(samples) > 1:
        print('Mislabelled resources have led to', len(samples), '. Exiting now')
        sys.exit(1)
    sample = samples[0]

    # check if temporary and final output directories already exist
    fdir = 'raw'
    if os.path.isdir(fdir):
        print('Destination directory already exists:', fdir)
        sys.exit(1)
    tdir = 'xraw'
    if os.path.isdir(tdir):
        print('Temporary directory already exists:', tdir)
        sys.exit(1)

    wget_args = [
        'wget',
        '--no-http-keep-alive',
        '--header=Authorization: ' + apikey,
        '-c', '-q',
        '-t', '0',
        '-P', tdir,
    ]

    for resource in sample['resources']:
        wget_cmd = wget_args + [resource['url']]
        print('fetching', wget_cmd[-1].rsplit('/', 1)[-1])
        # print('calling wget: %s' % (wget_args))
        res = subprocess.run(wget_cmd)
        if res.returncode: # bad is non-zero
            print(wget_cmd[0], 'returned', res.returncode)
            sys.exit(res.returncode)

        # should check MD5 checksum

    print('Done fetching.')
    os.rename(tdir, fdir)
    print('Moved temporary to final directory:', fdir)
    return

if __name__ == '__main__':
    main()
