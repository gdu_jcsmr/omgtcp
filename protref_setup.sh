#!/bin/bash

# script to setup a protein reference and diamond index
# for OMG pipelines.
die () { echo "$@"; exit 1 ; }
[ -z "$WORKDIR" ] && die WORKDIR environment variable is not defined.
DATADIR=~/omg/data
defref=marsupial
refx=${1:-$defref}
[ $# -le 2 ] || die "usage: $0 [reference [sampledir]]
	default reference is $defref
        default sampledir is ."

declare -a refv=($( cd $DATADIR ; ls *TargetsProt.fa | grep -i "^$refx" ))

[ ${#refv[@]} -gt 1 ] && {
    echo found ${#refv[@]} references for $1
    for f in ${refv[@]}
    do
      echo '   ' $f
    done
    die
}

[ -z "$refv" ] && die No reference found for $refx


bn=${refv%.fa}
[ "$refv" = $bn.fa ] || die "$refv - reference file must end with .fa"

# $sd is sample directory
sd=${2:-$PWD}	# default is current directory
# get absolute path - assumes file is in $WORKDIR if path isn't absolute
[ "${sd:0:1}" = / ] || sd=$WORKDIR/$sd

echo Installing reference $refv in $sd

set -x 

# Step 1 - link this reference.
# Link the xxx.fa files to this directory. 
# Create split files and diamond reference.

cd $WORKDIR
mkdir -p refs
cd refs
make -f ~/omg/code/Makeref REF=${bn%TargetsProt}

[ $sd = $WORKDIR/refs ] && exit	# all done - this is not a sample directory
[ ${sd%/*} = $WORKDIR ] || die Sample directory not in working directory.

# Step 2 - link reference files into sample directory
cd $sd
rm -rf ./protref* ./dnaref.fa
for x in .fa .dmnd ''
do
  ln -s $WORKDIR/refs/$bn$x ./protref$x || die "Unable to link $WORKDIR/refs/$bn$x to ./protref$x"
done
ln -s $WORKDIR/refs/${bn%Prot}DNA.fa ./dnaref.fa





