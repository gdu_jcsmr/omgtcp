#!/usr/bin/env python3
"""
Created on Mon Sep 17 13:54:00 2018

@author: Bob Buckley ABC JCSMR

pick a FASTA record by name from a FASTA file of sequences
Handle the direction and pick the exon sequence and the surrounding sequences

Note: this sccript reads the RYO records from exonerate
and adds key information to the comments part of FASTA headers
so later we can extract just the exon (or the flanking region).
"""

import sys
import argparse
import collections

import fasta

version="1.0"   # 19-9-2018

Target = collections.namedtuple('Target', 'id start end perc qstart qend'.split())
        
def mkTarget(lx):
    "convert our ryo output from exonerate program"
    fld = lx.rstrip().split('\t')
    assert len(fld)>=7
    start, end, qstart, qend  = map(int, fld[3:7])
    perc = float(fld[1])
    # should use namedtuple
    return Target(fld[8], start, end, perc, qstart, qend)
    
        
def pick(fncontigs, tgtname):
    # fncontig is the filename of the SPAdes contig file: form is <target>-t/contig.fasta
    # tgtname = fncontigs.split('-',1)[0] # get target name from contig filename
    global perc, minlen
    # filter here since exonerate --percent doesn't seem to work
    tgtall = [mkTarget(lx) for lx in sys.stdin]
    tgts = [x  for x in (tgtall) if x.perc>=perc and abs(x.end-x.start)>=minlen]
    if not tgts:
        print(tgtname+':', "No accepted contigs: minlen =", minlen, "match =",str(perc)+'%', file=sys.stderr)
        print([(abs(x.end-x.start), x.perc) for x in tgtall], file=sys.stderr)
        return
    tgt = tgts[0]
    hdr, seq = next(s for s in fasta.fasta_reader(fncontigs) if s[0].startswith(tgt.id))
    start, end = tgt.start, tgt.end
    if end<start: # need to reverse the config
        assert start<=len(seq)
        # tx = str.maketrans("ACGT", "TGCA", " -.\n")    
        tx = str.maketrans("ACGT", "TGCA")    
        seq = seq[::-1].translate(tx)
        start, end = len(seq)-start, len(seq)-end
    assert start<end
    # include the exon start and end in the header (comments)
    hdr = ' '.join(('>'+tgtname, tgt.id, str(start), str(end), "%.2f%%"%tgt.perc, str(tgt.qstart), str(tgt.qend)))

#    # output a FASTA file with just the exon
#    with open(tgtname+"-exon.fasta", "wt") as dst:
#        print(hdr, file=dst)
#        print(seq[start:end], file=dst)
        
    # output the whole chosen contig to stdout in FASTA format
    with open(tgtname+"-c.fasta", "wt") as dst:
        print(hdr, file=dst)
        print(seq, file=dst)
     
    # could (should?) output the GFF file here
    return
    
def main():
    parser = argparse.ArgumentParser(description="ABC script to pick the best contig for a protein target")
    parser.add_argument("-v", "--version", action="store_true", help="exit after displaying version number")
    parser.add_argument("-p", "--percent", type=float, default=70.0 , help="percent match/identical bases in exon (default=70%)")
    # Cameron gave me this defaults - 60 codons in length
    parser.add_argument("-l", "--length", type=int, default=180, help="minimum exon length in bases (default=180)")
    parser.add_argument("contigfn", help="name of the contig file")
#    parser.add_argument("protreffn", help="name of the target protein file")
    parser.add_argument("target", help="basename for the output files")
    args = parser.parse_args()

    global version
    if args.version:
        print(sys.argv[0].rsplit('/',1)[-1], '-', "Version", version, file=sys.stderr)
        return 0

#    prothdr, protseq = next(fasta.fasta_reader(args.protreffn))
    global perc, minlen
    perc = args.percent
    minlen = args.length

    pick(args.contigfn, args.target)
    return
    
if __name__=="__main__":
    main()
    

    