#!/usr/bin/env python3

"""
Created on Wed Mar 15 10:31:03 2017

@author: Bob Buckley, ANU Bioinformatics Consultancy, John Curtin SChool of Medical Research, ANU

dorun module, version 2 - written in python 3.x

This version uses a .dorun file to list target files. If the .dorun is locked
for exclusive access then another dorun pipleine is operating in the current
directory, so this invocation should stop.
If a .dorun file is present and readable then the files listed in it
are partial results so they should be removed.
Whenever a new dorun step is started, the names of its targets should be
written to a new (exclusively accessed) .dorun file. When the step finishes
successfully, the .dorun file is removed.

Uses some make(1) approaches ... but understand intermediate/temporary files.

need to add a 'lookup' function for commands ... locates the command files on the system
needs a retry mechanism for steps.
"""
myshell="/bin/bash" # for subprocess calls

import os
import sys
import subprocess
import collections
import fcntl
import shlex
import datetime
import glob
import argparse

class PipeFail(Exception):
    pass

def init_dorun():
    """
    Called once at the start of a dorun pipeline.
    If there is a .dorun file present, and it can be accessed, then
    read it and remove all the files it lists as they are incomplete files.
    Then remove the .dorun file.
    """
    if not os.path.isfile(".dorun"):
        return

    global args
    if 'd' in args.debug:
        print("file .dorun exists!")
    _ff = collections.namedtuple('FileFunctions', 'test remove'.split())
    ffs =  { 'f':_ff(os.path.isfile, os.remove), 'd':_ff(os.path.isdir, os.rmdir)}
    # file exists and can be read - remove listed files if they exist
    with open(".dorun") as src:
        pid = next(src) # first line contains
        if 'd' in args.debug:
            print(".dorun file from PID =", pid, file=sys.stderr)
        for r in src:
            flag, fn = r.split()
            if ffs[flag].test(fn):
                if 'd' in args.debug:
                    print("  removing", {'f':'file', 'd':'directory'}[flag], fn)
                ffs[flag].remove(fn)
    if 'd' in args.debug:
        print("remove .dorun")
    os.remove(".dorun")
    return

def dostat(fns, func=lambda x:os.stat(x).st_mtime):
    """
    return the list of values, like map(func, fns), for a list of files fns
    and a list of names of any missing files.
    """
    stats, missing = [], []
    for fn in fns:
        try:
            stats.append(func(fn))
        except:
            missing.append(fn)
    if missing:
        pc = 's' if len(missing)>1 else ''
        raise PipeFail("missing file"+pc, missing)
    return stats

def timecheck(inputs, targets):
    """
    check the modification times of a (possibly empty) set of input files
    relative to a non-empty set of target files.
    Should only be called if all files are present; i.e. there are no inputs or targets missing
    """
    first_out = min(dostat(targets))
    last_in = max(dostat(inputs))
    if last_in < first_out:
        print("times: last in",
              datetime.datetime.utcfromtimestamp(last_in).strftime("%Y/%m/%d %H:%M:%S"),
              "first out", datetime.datetime.utcfromtimestamp(first_out).strftime("%Y/%m/%d %H:%M:%S")
             )
    return last_in < first_out

def _dorun(s):
    """
    Run Step s
    s is Step with attributes command, inputs, targets, ... (see above)
    """

    # check if targets exist and are current
    cmdstr = s.command if isinstance(s.command, str) else ' '.join(s.command)
    # cmdname = s.command.split(None, 1)[0] if isinstance(s.command, str) else s.command[0]
    print("Step: ", end='')
    if s._runcheck():
        return
    # print("create .dorun")
    try:
        dorunf = open(".dorun", mode="xt") # exclusive access is important
        fcntl.lockf(dorunf.fileno(), fcntl.LOCK_EX|fcntl.LOCK_NB)
    except:
        print("can't create .dorun exclusively!", file=sys.stderr)
        sys.exit(1)
    print(os.getpid(), file=dorunf)
    for fn in s.targets:
        print("f", fn, file=dorunf)
    for dn in s.target_dirs:
        print("d", dn, file=dorunf)
    dorunf.flush()

    # run the command
    rfd, to = None, None
    print()
    print("+", cmdstr, flush=True)
    if s.time:
        cmdstr = ' '.join((s.time, cmdstr))
    # use bash to run - bash has better substitutions
    global myshell
    resx = subprocess.run([myshell, '-c', cmdstr], stdout=rfd, timeout=to, check=False)
    res = resx.returncode
    if res:
        print("  returned "+str(res), flush=True)
    # subprocess.run(['ls', '-ltr'])

    def fail(*args):
        # like make - remove targets for failed commands
        for fn in s.targets:
            os.remove(fn)
        PipeFail(*args)

    if res!=0: # check exit status of the command
        fail("non-zero exit status", res)

    # check that all the targets are present on completion of a command
    missing = [fn for fn in s.targets if not (os.path.isfile(fn) or os.path.isdir(fn))]
    if missing:
        print("Current directory:", os.getcwd(), "  Missing target files:")
        for fn in missing:
            print("    '"+fn+"'")   
        print(flush=True, end='')
        fail("missing targets:", missing)

    # check that targets are non-empty unless listed in allow_empty
    nonmt = tuple(fn for fn in s.targets if fn not in s.allow_empty)
    szs = map(os.path.getsize, s.targets)
    efs = [fn for sz, fn in zip(szs, nonmt) if not sz]
    if efs: # fails if any empty files
        print("  empty target file(s):")
        for fn in efs:
            print("   ", fn)
        print(flush=True, end='')
        fail("empty target file(s):", efs)

    # remove before close to ensure exclusive access
    # print("remove .dorun")
    os.remove(".dorun")
    dorunf.close()
    return

# StepSpec should be an abstract class - I'm not sure how to do that
_XStepSpec = collections.namedtuple('_XStepSpec', 'pname inputs targets allow_empty target_dirs'.split())
class StepSpec(_XStepSpec):
    def __new__(cls, pname, inputs, targets, allow_empty, target_dirs):
        # allow some arguments to be single strings or a list of stings
        inputs, targets, allow_empty, target_dirs = \
           ([os.path.expanduser(z) for z in ([x] if isinstance(x, str) else x)] for x in (inputs, targets, allow_empty, target_dirs))
        tgtset = set(targets)
        if len(targets)!=len(tgtset):
            print("Warning: duplicate filename(s) in targets!") # not really a problem
        bad_empty_allowed = [x for x in allow_empty if x not in tgtset] # validation
        if bad_empty_allowed:
            raise PipeFail("allowed empty file is not in targets: "+' '.join(bad_empty_allowed))
        return super(StepSpec, cls).__new__(cls, pname, inputs, targets, allow_empty, target_dirs)

    def _runcheck(self):
        # this doesn't handle target directories properly ... yet
        # they are not being used on the OMG pipelines
        print(self.pname, '', end='')
        assert self.targets+self.target_dirs, "step must have target(s)."
        if self.targets:
            missing = [t for t in self.targets if not(os.path.isfile(t) or os.path.isdir(t))]
            if self.inputs:
                srctime = max(os.stat(fn).st_mtime for fn in self.inputs)
                replace = [fn for fn in self.targets if fn not in missing and os.stat(fn).st_mtime < srctime]
            else:
                replace = self.targets
            if not (missing or replace):
                print('-- skipped.')
                return True
                
        print("-- starting at", datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
        if self.inputs:
            print("  inputs:")
            for fn in self.inputs:
                print('     ', fn)
        else:
            print('-- no inputs, running this step is mandatory.')
            
        print("  targets (c - create, r- replace):")
        for tn in self.targets:
            print(''.join(flag if tn in fns else '' for flag, fns in zip('cr', (missing, replace))).rjust(5), tn)
        if self.target_dirs:
            print("  target_dirs:", self.target_dirs)

        return False
        # return bool(self.inputs) and not missing and timecheck(self.inputs, self.targets)

class Step(StepSpec):
    """
    Specify a pipeline step that runs a command.
    """
    time = None # allow for setting a global "time" command - could be "time" or "/usr/bin/time"

    def __new__(cls, pn, command, inputs, targets, allow_empty=[], target_dirs=[]):
        return super(Step, cls).__new__(cls, pn, inputs, targets, allow_empty, target_dirs)

    def __init__(self, pn, command, inputs, targets, allow_empty=[], target_dirs=[]):
        self.command = command
        return

    def dorun(self):
        _dorun(self)
        return
    def show(s):
        cmdstr = s.command if isinstance(s.command, str) else ' '.join(s.command)
        print("Step -", s.pname)
        targets = [x+('*' if x in s.allow_empty else '') for x in s.targets]
        for name, xs in (("Inputs", s.inputs), ("Targets", targets), \
                            ("Directories", s.target_dirs)):
            if xs:
                print(name, "(%d):"%len(xs), ' '.join(xs))
        print("+", cmdstr, flush=True)
    def doclean(self):
        return

class Pipeline(StepSpec):
    """
    A pipleline is a series of sub-pipelines or command steps
    that run in sequence.
    """
    def __new__(cls, pn, steps, inputs, targets, allow_empty=[], target_dirs=[]):
        return super(Pipeline, cls).__new__(cls, pn, inputs, targets, allow_empty, target_dirs)
        # return StepSpec.__new__(cls, inputs, targets, allow_empty, target_dirs)

    def __init__(self, pn, steps, inputs, targets, allow_empty=[], target_dirs=[]):
        self.steps = steps
        return

    def dorun(self):
        """
        perform a series of dorun steps.
        spec is a set/list of Step objects
        targets lists the stage targets - inputs are inferred
        """
        print("Pipeline: ", end='')
        assert self.targets+self.target_dirs, "no target file(s) or directory for pipeline."
        # code needs to build a total order from the partial order.
        # for now, we just do the steps
        if self._runcheck():
            return
#        else:
#            if all((os.path.isfile(t) or os.path.isdir(t)) for t in self.targets):
#                print("input vs targets =", min(dostat(self.inputs)), 'vs', min(dostat(self.targets)))
#            else:
#                print("missing targets:", [t for t in self.targets if not os.path.isfile(t) and not os.path.isdir(t)])
        for n, s in enumerate(self.steps, start=1):
            print("\nStep", n, s.pname, "starting.", flush=True)
            s.dorun()
            print("Step", n, '-', s.pname, "done.", flush=True)
        # remove temporaries
        print("Pipeline", self.pname, "- done at", datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S"), end='')
        tns = sorted(set(fn for s in self.steps for fn in s.targets if fn not in self.targets))
        if tns:
            print(" removing temporaries ...")
            for tn in filter(os.path.isfile, tns):
                print("    remove", tn)
                os.remove(tn)
            for fn in filter(os.path.isdir, tns):
                print('    remove directory', fn)
                os.rmdir(fn)
        else:
            print()
        print(flush=True)
            # need to do directories too?
        return

    def show(self):
        "display what this pipeline does"
        print("Pipeline", self.pname, ". steps =", ' '.join(s.pname for s in self.steps))
        if self.inputs:
            print("Inputs:")
            for fn in self.inputs:
                print("   ", fn)
        print("================================")
        print()
        for s in self.steps:
            s.show()
            print()
        tns = sorted(set(fn for s in self.steps for fn in s.targets if fn not in self.targets))
        if tns:
            print("remove temporaries", "(%d):"%len(tns))
            for fn in tns:
                print('   ', fn)
        print()
        print("================================")
        print("Targets:")
        for fn in self.targets:
            print("   ", fn)
        print("Pipeline", self.pname, "- end.", flush=True)
        return

    def doclean(spec, targets):
        """
        Clean the results of a pipeline ...
        Get rid of old results - it is used if a pipeline needs
        to be run with a different tool or if different command arguments are needed.
        """
        for s in spec:
            s.doclean()
        tmps = set(fn for fn in targets+[x for xs in spec for x in xs.targets])
        for fn in tmps:
            # may not work for directories
            if os.path.exists(fn):
                os.remove(fn)
        return

import configparser

def unique(xss):
    "sort and remove duplicates from a sequence, preserve order"
    xs = tuple(xss)
    return [x for i, x in enumerate(xs) if x not in xs[:i]]
    
def getconfig(fns, config=None):
    # read pipeline configuration
    global args
    def mysplitext(fn):
        "get the basename without leading directories or trailing extension - handle .gz files"
        bn, ext = os.path.splitext(os.path.split(fn)[1])
        if ext=='.gz':
            b, e = os.path.splitext(bn)
            bn, ext = b, e+ext
        return bn, ext
    if not config: # allow config to be primed externally
        config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    if 'c' in args.debug:
        print("Reading pipeline configuration files.")
        print(" ", fns)
    
    for fn in fns:
        cfgfiles = config.read(os.path.expanduser(fn))
        if 'c' in args.debug:
            if cfgfiles:
                print("   ", fn)
        for pn, cs in config.items():
            pnflag = pn
            ks = list(cs.keys())
            for k in ks: # can't step through cs.item() - changes not allowed for an OrderedList
                val = cs[k]
                if k.endswith('?'):
                    if k[:-1] not in cs:
                        fns = [fn for pat in val.split() for fn in glob.glob(pat)]
                        cs[k[:-1]] = ' '.join(fns)
                        if 'c' in args.debug:
                            print()
                            print("  x[%s] >>"%k, cs[k[:-1]])
                    continue
                ks = k.split('+', 1)
                if len(ks)==2 and ks[1]!='': # k=='command+' is still special
                    if ks[0] in cs:
                        continue
                    if pnflag:
                        if 'c' in args.debug:
                            print()
                            print("substitutes in", pn)
                        pnflag = None
                    basenames = tuple(unique(mysplitext(fn)[0] for fn in cs[ks[1]].split()))
#                    print('basenames from %s[%s] ='%(pn, ks[1]), basenames)
                    vs = val.split()
                    cs[ks[0]] =' '.join((' '.join(vx%arg for arg in basenames) if '%s' in vx else vx) for vx in vs ).replace('$', '$$').replace('\n', ' ')
                    if 'c' in args.debug:
#                        print("  x[%s] ="%k, val)
                        print("  basenames =", basenames)
                        print("  x[%s] ="%ks[0], cs[ks[0]])
            if 'c' in args.debug:
                print('  cfg part:', pn)
                for k in ['input', 'output', 'command', 'steps']: #cs.keys():
                    if k in cs:
                        print('    ', k, '=', cs[k])

    print(flush=True)
    return config

def main(fnx, config=None):
    parser = argparse.ArgumentParser(description="ABC .ini file interpreter")
    parser.add_argument("-v", "--version", action="store_true", help="exit after displaying version number")
    parser.add_argument("-n", "--show", action="store_true", help="display process, don't run")
    parser.add_argument("-d", "--debug", default="di", help="debug flags (d - .dorun file, i - initialise, c - full commands, g - global)")
    parser.add_argument("sampdir", help="name of the sample directory (with raw/* files)")
    global args
    args = parser.parse_args(sys.argv[2:])
    if 'g' in args.debug:
        print("argv = ", sys.argv)

    sample_dir = args.sampdir
    if 'g' in args.debug:
        print('sample_dir =', sample_dir)
        print('show =', args.show)
    # run in the sample_dir
    if sample_dir!='.':
        os.chdir(sample_dir)
    init_dorun()

    if 'c' in args.debug:
        print("ini files =", fnx)
    config = getconfig(fnx, config)

    pipeparts = {}
    def argfix(s):
        sx = os.path.expanduser(s) if s.startswith('~') else s
        return sx # if sx[0] in '>|&;<' else shlex.quote(sx) # not sure that shlex.quote is best
    def mkstep(n, x):
        #  should also do output directories, ...
        # cmd, inp, out = [[argfix(s) for s in shlex.split(x.get(arg, ''))] for arg in ('command', 'input', 'output')]
        cmd = x['command'].replace('\n', ' ')
        inp, out = [shlex.split(x.get(arg, '')) for arg in ('input', 'output')]
        return Step(n, cmd, inp, out)
    def pipesteps(mypn, cfg):
        psteps = []
        for arg, inarg, outarg in zip(*tuple(cfg[x].split() for x in ('args', 'input', 'output'))):
            newcfg = dict(list(cfg.items())+[('input', inarg), ('output', outarg)])
            newcfg['command'] = ' '.join(x%arg if '%s' in x else x for x in cfg['command+'].split())
            psteps.append(mkstep(mypn, newcfg))
        return psteps

    # build data structures ... 
    tgts = set(*config['pipeline']['output'])
    dst, ins = {}, set(())
    for pn, cs in config.items():
        if not any(c in cs for c in ['command', 'command+']):
            continue
        ins.update(*cs['input'])
        for tgt in cs['output']:
            assert tgt not in dst # unique outputs
            dst[tgt] = pn
    outs = set(dst.keys())
    # temporary files are outputs that are not pipeline outputs
    # pipeline inputs are inputs that are not outputs
    print('pipeline inputs:', ins-outs)
    # should check that all the inputs are present
    print('pipeline outputs:', tgts)
    unk = tgts-outs
    if len(unk):
        raise PipeFail('unknown outputs', unk)
    
    # should check that dependencies are an acyclic graph
    # the run processes to create missing outputs
    
    # find missing or out-of-date targets
    filetimes = dict((fn, os.stat(fn).st_mtime) for fn in ins | outs if os.isfile(fn))
    missing = tuple(fn for fn in ins-outs if fn not in filetimes)
    if missing:
        raise PipeFail('pipeline input missing', missing)
    
    inptime = max(filetimes[fn] for fn in ins-outs)
    def myinputs(ts):
        cmds = set(dst[t] for t in ts if t in dst)
        myinps = set(i for c in cmds for i in config[c]['inputs'])-ts
        if myinps:
            myinps |= myinputs(myinps)
        return myinps 
    
    
        
    unk = 0
    for pn, cs in config.items():
        if 'command' in cs:
            pipeparts[pn] = mkstep(pn, cs)
        elif 'command+' in cs:
            inputs, targets = [[argfix(s) for s in shlex.split(cs.get(arg, ''))] for arg in ('input', 'output')]
            steps = pipesteps(pn, cs)
            if 'c' in args.debug: #len(steps)>1:
                for s in steps:
                    print('   !!!', s.command)
            pipeparts[pn] = Pipeline(pn, steps, inputs, targets)
        elif 'steps' in cs:
            # check that all inputs are known
            inputs, targets, stepnames = [[argfix(s) for s in shlex.split(cs.get(arg, ''))] for arg in ('input', 'output', 'steps')]
            for n, sn in enumerate(stepnames):
                # print("    stepname:", sn, "  input:", config[sn]['input'])
                visinp = frozenset(inputs+[i for s in stepnames[:n] for i in config[s]['output'].split()])
                for inp in map(argfix, shlex.split(config[sn].get('input', ''))):
                    if inp not in visinp:
                        if 'c' in args.debug:
                            print('step', sn+':', 'input source for', inp, 'is unknown.')
                        unk += 1
            pipeparts[pn] = Pipeline(pn, [pipeparts[x] for x in stepnames], inputs, targets)
    if unk:
        print(unk, "improperly constructed pipeline(s) - aborting.")
        sys.exit(1)

    px = pipeparts['pipeline']
    if args.show:
        px.show()
    else:
        try:
            px.dorun()
        except PipeFail as e:
            print("Pipeline failed:", e)
    return

if __name__=="__main__":
    # show arguments - test code
    for n, s in enumerate(sys.argv):
        print(str(n)+':', s)
    # argv[1] is the rest of the #! line after calling this code - may have multiple .ini files
    main(shlex.split(sys.argv[1])+[sys.argv[2]])
