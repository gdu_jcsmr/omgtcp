"""
FASTA and FASTQ parser module
Bob Buckley, ANU Bioinformatics Consultancy, 1/8/2015
"""

# The guts of this uses Python's itertools.groupby() to implement a FASTA parser
# and zip on a generator for a FASTQ parser.
# It makes extensive use of Python's generators. It should be quite efficient in
# most applications. 

# Note: functions starting with _ are meant to be private to the module.

import itertools
import gzip

def _hdrfix(hdr):
    "'fix' a header - drop the first character (the '>' or '@') then strip()"
    return hdr[1:].strip()

def in_fasta(lines):
    """FASTA parser
    
    Argument is an open FASTA file or a list/generator of lines from a FASTA file.
    Note: if the file has empty sequences (adgacent header lines), the leading headers are lost.
    """
    def last(g):
        "last value from an iterator - like list(g)[-1]."
        # note: we expect g will always have exactly one value so we could use next()
        for x in g:
            pass
        return x
    # the first part of the tuple (below) extracts the sequence header info
    # the second joins all the lines of subsequence
    g = (_hdrfix(last(ls)) if b else ''.join(s.strip() for s in ls)
         for b, ls in itertools.groupby(lines, lambda x:x.startswith('>')))
    # zip(g,g) is a pythonic trick for making pairs from a sequence/generator
    # using itertools.izip (instead of zip) reads on demand 
    return zip(g,g)

def in_fastq(lines):
    """FASTQ parser
    
    Argument is an open FASTQ file or a list/generator of lines from a FASTQ file.
    Note: this doesn't check for leading '@' or '+' in 1st & 3rd lines
    """
    return ((_hdrfix(h), s.strip(), q.strip()) for h,s,x,q in itertools.zip_longest(lines, lines, lines, lines))

def _reader(filename, gg):
    """generator that keeps a file open while it's still in use
    
    gg is a function  that returns/generates a generator
    """
    myopen = (lambda x: gzip.open(x, "rt")) if filename.endswith('.gz') else open
    with myopen(filename) as src:
        for r in gg(src):
            yield r
    return

def fasta_reader(fn):
    "returns a FASTA record generator for the FASTA file named fn"
    return _reader(fn, in_fasta)
    
def fasta_dict(fn):
    return dict((sid.split(None,1)[0], seq) for sid, seq in fasta_reader(fn))

def fastq_reader(fn):
    "returns a FASTQ record generator for the FASTQ file named fn"
    return _reader(fn, in_fastq)

# the following is test code

if __name__=="__main__":

    for fa in fasta_reader('files/mt.fasta'):
        print(fa)
    
    for fq in fastq_reader('files/example.fq'):
        assert len(fq[1])==len(fq[2])
        print(fq)
