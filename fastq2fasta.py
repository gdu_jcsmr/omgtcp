#!/usr/bin/env python3
"""
read a FASTQ file, output a FASTA file

Bob Buckley, ABC JCSMR, ANU
26/10/2017

I wrote this because the other versions available on the internet
seemed too challenging to use ... and didn't always allow .fastq.gz input.

Maybe needs ...
* a --output option.
* allow multiple input files - work like the cat command
"""

import os
import sys
import gzip
import collections as cx

def myopen(fn, mode='r'):
    "open a file in text mode"
    if fn.endswith('.gz'):
#        cmd = 'gzip -9 >' if mode=='w' else 'ungzip <' # or 'gzip -dc <'
#        cmd = 'pigz -p 2 --best >' if mode=='w' else 'unpigz -p 2 <' # or 'gzip -dc <'
#        return os.popen(cmd+fn, mode) 
        if mode=='w':
            # cmd = 'pigz -p 2 --best >'
            cmd = "gzip -9 >" # this is about as fast as it goes
            return os.popen(cmd+fn, mode) 
        mx = mode if any((c in mode) for c in "bt") else mode+'t'
        # default compresslevel==9
        return gzip.open(fn, mx)
#    elif fn=='-':
#        return sys.stdout if 'w' in mode else sys.stdin
    else:
        return open(fn, mode)

qrecs = 'rid comments seq qual'
FastqRecx = cx.namedtuple('FastqRecx', qrecs.split())

class FastqRec(FastqRecx):
    "basic FASTQ record with added method(s)"
    def str(self):
        "string from a FASTQ record"
        hdr = '@'+self.rid
        if self.comments: # avoids adding extra space if self.comment is empty
            hdr = hdr+' '+self.comments
        return '\n'.join((hdr, self.seq, '+', self.qual))
        
def reader(fn):
    "reader for FASTQ files"
    with myopen(fn) as src:
       for r in zip(src, src, src, src):
           hr = r[0].rstrip()[1:].split(None, 1)
           seq, qual = [r[i].rstrip() for i in (1,3)]
           yield(FastqRec(hr[0], hr[1] if len(hr)>1 else None, seq, qual))
    return       

def main():
    for r in reader(sys.argv[1]):
        print('>'+r.rid, r.comments)
        print(r.seq)

if __name__=="__main__":
    assert len(sys.argv)==2, "Missing input file"
    main()
