#!/usr/bin/env python3
"""
Created on Mon Mar 26 14:07:16 2018

@author: bobbuckley
"""

version="1.0" # 26-MAR-2018

import os
import sys
import argparse
import collections
import subprocess

import FastxLib as fx

# define an upper level 'pair tuple' for paired-end reads
_PT = collections.namedtuple('Pair', 'seq0 seq1 seq2 rid q1 q2'.split())

class SeqPair(_PT):
    
    def __new__(cls, r1, r2):
        # combine the first part of the paired ends
        xlen = min(map(len, (r1.seq, r2.seq)))
        fseq = bytes(ord(c) for cs in zip(r1.seq, r2.seq) for c in cs).decode() 
        return super(SeqPair, cls).__new__(cls, fseq, r1.seq[xlen:], r2.seq[xlen:], r1.rid, r1.qual, r2.qual)

    def __str__(s, cs=None):
        return '\t'.join((s.seq0, s.seq1, s.seq2, s.rid, s.q1, s.q2))
    
    @staticmethod
    def from_str(line):
        args = tuple([SeqPair]+line.split('\t'))
        return super(SeqPair, SeqPair).__new__(*args)
    
    def fastqs(self, cs):
        return fx.FastqRec(self.rid, cs[0], self.seq0[0::2]+self.seq1, self.q1), \
              fx.FastqRec(self.rid, cs[1], self.seq0[1::2]+self.seq2, self.q2)  
              
def pair_write(fq1, fq2, f1, f2):
    for rec, f in zip((fq1, fq2), (f1, f2)):
        print(rec, file=f)
    return
        

def main():
    """Quick Deduplicater - remove duplicate (paired-end) reads
    
    Bob Buckley, ANU Bioinformatics Consultancy, 26/3/2018
    
    Create a combined records for paired end read - and adjust quality.
    Write to a sort process ... close the output when done.
    Read the sort process deduping as it goes, and writing deduped records.
    Note: using the external sort function avoids problems with large files.
    Note: comments are not put into the intermediate file as comments are 
    (expected/required to be) constant. And R1 and R2 IDs have to be the same.
    """
    parser = argparse.ArgumentParser(description="ABC script to remove duplicate reads from paired-end read files")
    parser.add_argument("-v", "--version", action="store_true", help="exit after displaying version number")
    parser.add_argument("-t", "--temp", default="./tmp", help="filename for temporary directory (default=tmp)")
#    parser.add_argument("-o", "--output", required=True, help="filename for R1 non-duplicate reads result (compressed .gz file is allowed)")
    parser.add_argument("output", help="filename for R1 non-duplicate reads result (compressed .gz file is allowed)")
    parser.add_argument("files", nargs='+', help="R1_fastq_input_files (compressed .gz files are accepted)")
    args = parser.parse_args()

    global version
    print(sys.argv[0].rsplit('/',1)[-1], '-', "Version", version, file=sys.stderr)
    if args.version:
        return 0
    
    # should create directory args.temp if it isn't already there
    mktmp = False
    if not os.path.isdir(args.temp):
        os.mkdir(args.temp)
        mktmp = True
    # because it is sort, we can have a send phase and a receive phase - no subprocesses required
    mysort = subprocess.Popen(["sort", '-T', args.temp], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True) 
	# encoding='utf-8')

        
    # Phase 1 - combine records and output them to sort
    rrcnt, cs = 0, None
    dst = mysort.stdin
    for fn in args.files:
        rcnt = 0
        fns = fn, fn.replace('_R1', '_R2')
        rdr1, rdr2 = map(fx.fqreader, fns)
        for s1, s2 in zip(rdr1, rdr2):
            # fix old style Illumina paired-end data for our pipeline
            # move the pair number (1 or 2) into the comments part of the header
            if s1.rid[-1]=='1' and s2.rid[-1]=='2' and s1.rid[-2]==s2.rid[-2] and s1.rid[-2] in "./:":
                s1 = fx.FastqRec(s1.rid[:-2], '1', s1.seq, s1.qual)
                s2 = fx.FastqRec(s2.rid[:-2], '2', s2.seq, s2.qual)
            if cs:
                assert cs == (s1.comments, s2.comments)
            else:
                # set initial values for comments
                cs = s1.comments, s2.comments
            assert s1.rid and s1.rid==s2.rid
            sp = SeqPair(s1, s2)
            print(sp, file=dst)
            rcnt += 1
#            if rcnt%500000==0:
#                print(rcnt, "records", flush=True, file=sys.stderr)
        print(rcnt, "reads in", fn, flush=True, file=sys.stderr)
        rrcnt += rcnt
    dst.close()
    if len(args.files)>1:
        print(rrcnt, "total reads", flush=True, file=sys.stderr)
    
    fnout1 = args.output
    fnout2 = fnout1.replace('_R1', '_R2')
    
    # Phase 2 - read sorted records and deduplicate
    def seqorder(fq1, fq2):
        ss, sl = (fq1.seq, fq2.seq) # short sequence & longer sequence
        if len(sl)<len(ss): #if not the right way, then swap them
            ss, sl = sl, ss
        return ss, sl
    wcnt, dcnt = 0, 0 
    src = mysort.stdout
    with fx.myopen(fnout1, "w") as dst1, fx.myopen(fnout2, "w") as dst2:
        print('Deduping ...', end=' ', flush=True, file=sys.stderr)
        pp = SeqPair.from_str(next(src).rstrip()) # it takes a while for the first record to emerge
        print('sorted reads.', flush=True, file=sys.stderr)
        prevp1, prevp2 = pp.fastqs(cs)
        xcnt = 1
        for rx in src:
            xcnt += 1
            np = SeqPair.from_str(rx.rstrip())
            nextp1, nextp2 = np.fastqs(cs)
            def prmatch():
                if np.seq0.startswith(pp.seq0):
                    s1s, s1l = seqorder(prevp1, nextp1)
                    s2s, s2l = seqorder(prevp2, nextp2)
                    return (s1s, s1l, s2s, s2l) if s1l.startswith(s1s) and s2l.startswith(s2s) else None 
                else:
                    return None
            seqs = prmatch()
            if seqs: # combine sequences
                # combine qualities
                s1s, s1l, s2s, s2l = seqs
                q1, q2 = (bytes(max(ord(c1), ord(c2)) for c1, c2 in zip(fq1.qual, fq2.qual)).decode()+fq1.qual[xlen:]+fq2.qual[xlen:] for fq1, fq2, xlen in ((prevp1, nextp1, len(s1s)), (prevp2, nextp2, len(s2s))))
                prevp1 = fx.FastqRec(nextp1.rid, nextp1.comments, s1l, q1) 
                prevp2 = fx.FastqRec(nextp2.rid, nextp2.comments, s2l, q2) 
                dcnt += 1
            else:
                # write the previous pair and move on
                pair_write(prevp1, prevp2, dst1, dst2)
                wcnt += 1
                pp, prevp1, prevp2 = np, nextp1, nextp2
            if xcnt%500000==0:
                print(xcnt, "reads processed.", dcnt, "duplicates (%.2f%%)"%(dcnt/xcnt*100), flush=True, file=sys.stderr)

        pair_write(prevp1, prevp2, dst1, dst2)
        wcnt += 1
        
#        print(xcnt, "reads processed.", file=sys.stderr)
        print(dcnt, "duplicates found (%.2f%%)"%(dcnt/xcnt*100), file=sys.stderr)
        assert wcnt+dcnt == rrcnt

    mysort.wait()
    if mysort.returncode:
        print("Return code =", mysort.returncode, file=sys.stderr)
        sys.exit(mysort.returncode)
        
    if mktmp:
        os.rmdir(args.temp)
        
    return
        
 
if __name__=="__main__":
    main()
    print("Done.", file=sys.stderr)

