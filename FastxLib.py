#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 14:25:21 2018

@author: Bob Buckley
"""
import os
import gzip
import collections as cx

def myopen(fn, mode='r'):
    "open a file in text mode"
    if fn.endswith('.gz'):
#        cmd = 'gzip -9 >' if mode=='w' else 'ungzip <' # or 'gzip -dc <'
#        cmd = 'pigz -p 2 --best >' if mode=='w' else 'unpigz -p 2 <' # or 'gzip -dc <'
#        return os.popen(cmd+fn, mode) 
        if mode=='w':
            # cmd = 'pigz -p 2 --best >'
            cmd = "gzip -9 >" # this is about as fast as it goes
            return os.popen(cmd+fn, mode) 
        mx = mode if any((c in mode) for c in "bt") else mode+'t'
        # default compresslevel==9
        return gzip.open(fn, mx)
#    elif fn=='-':
#        return sys.stdout if 'w' in mode else sys.stdin
    else:
        return open(fn, mode)
    
_FastqRecx = cx.namedtuple('_FastqRecx', 'rid comments seq qual'.split())

class FastqRec(_FastqRecx):
    def __str__(self):
        return '\n'.join(('@'+self.rid+' '+self.comments, self.seq, '+', self.qual))
        
def fqreader(fn):
    "reader for FASTQ files"
    with myopen(fn) as src:
       for r in zip(src, src, src, src):
           hr = r[0].rstrip()[1:].split(None, 1)
           seq, qual = [r[i].rstrip() for i in (1,3)]
           yield FastqRec(hr[0], hr[1] if len(hr)>1 else None, seq, qual)
    return       
