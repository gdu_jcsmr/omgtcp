#!/usr/bin/env python3
"""
Created on Sun Feb  4 23:41:47 2018

@author: Bob Buckley
Paired end reads check: ensure that the names of the pairs match
"""
import sys
import fasta

def main():
    fn1 = sys.argv[1]
    assert fn1

    pos =fn1.rfind('_R1')
    if pos==-1:
        # assume interleaved
        src1 = fasta.fastq_reader(fn1)
        src2 = src1
        print("Interleaved file:", fn1)
    else:
        fn2 = fn1[:pos+2]+'2'+fn1[pos+3:]
        print("fn1 =", fn1)
        print("fn2 =", fn2)
        src1, src2 = [fasta.fastq_reader(fn) for fn in (fn1, fn2)]
    for n, (r1, r2) in enumerate(zip(src1, src2), start=1):
        if r1[0]!=r2[0]:
            print(str(n)+':', 'header mismatch')
            print(r1[0])
            print(r2[0])
            print()
    print()
    print("did", n, "records.")
    return
    
if __name__=='__main__':
    main()
