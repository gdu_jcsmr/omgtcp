# -*- coding: utf-8 -*-

"""
summarise a series of fastqc results for the OMG project

Bob Buckley, ANU Bioinformatics Consultancy,
John Curtin School of Medical Research,
Australian National University
15/6/2017
"""
import os
import sys

import fastqcLib

def toicon(x):
    dx = {'PASS':'tick', 'WARN':'warning', 'FAIL':'error' }
    return '<img src="icon/'+dx[x]+'.png" width=15 />'

def dodir(dirname):
    # print("processing", dirname, file=sys.stderr)
    smry = fastqcLib.FastqcRec(dirname)
    pre =  "<a href='../"+dirname+"/fastqc_report.html' target='_blank'>"
    post = "</a>"
    return pre + ''.join(toicon(x[0]) for x in smry.summary) + post
    
def dorow(lab, dirs):
    tds = ''.join('<td>'+dodir(d)+'<br/>'+dodir(d.replace('L001', 'L002'))+'</td>' for d in dirs)
    res = [ "<tr>",
            "<td>", lab, "</td>",
            tds,
            "</tr>",
          ]
    return ''.join(res)

def main():
    """
    Collect a number of fastqc results into a single report
    """
    hdr = [ "<!DOCTYPE html>",
            "<html>",
            "<head>",
            "  <style>",
            "    th, td {",
            "              outline-color: lightgray; outline-style: solid; outline-width: 1px;",
            "              padding: 2px 10px; ",
            "    }",
            "  </style>",
            "</head>",
            "<body>",
            "  <h1>fastqc summaries</h1>",
            "  <table>",
            "      <tr><th rowspan=2 valign=bottom />Sample</th>",
            "          <th colspan=2>raw</th><th colspan=2>trimmed</th></tr>",
            "      <tr><th>R1</th><th>R2</th><th>R1</th><th>R2</th></tr>",
          ]
    tail = [ "  </table>",
             "</body>",
             "</html>",
           ]
    for lx in hdr:
        print(lx)
    # the command line has the names of all the samples Lane 1, raw, first end pair QC directories
    # it works out all the other directories from that ...
    for dir1 in sys.argv[1:]:
        lab = dir1.split('_L001',1)[0]
        # dirs = (os.path.join(d, x) for lane in (dir1, dir1.replace('_L001', '_L002')) for d in ('data-rqc', 'data-tqc') for x in (lane, lane.replace('_R1', '_R2')))
        dirs = (os.path.join(d, x) for d in ('data-rqc', 'data-tqc') for x in (dir1, dir1.replace('_R1', '_R2')))
        print("   ", dorow(lab, dirs))
    for lx in tail:
        print(lx)
    return
    
if __name__ == "__main__":
    main()