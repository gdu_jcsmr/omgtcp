
set -e
# fix python3 PATH on gduserv ??? - this needs to disappear
case $PATH in
  *python3* ) ;;
  * ) PATH=${PATH/python/python3} ;;
esac

rdir=report
dirs=$(cd data-rqc; ls -d *_L001_R1*_001_fastqc)

# put the basic icon files in a local directory
[ -d $rdir/icon ] || {
  mkdir -p $rdir/icon
  for x in $dirs
  do
    ln data-rqc/$x/Icons/[etw]*.png $rdir/icon/.
    break # after first directory
  done
}

# collect the summaries into one file
python3 code/qcsummary.py $dirs > $rdir/qcsummary.html
echo Done.
