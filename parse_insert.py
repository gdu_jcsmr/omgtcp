#!/usr/bin/env python3

import sys

"""
    Read histogram of insert sizes and return percentage
    of reads 130 bases or less, 130->190 bases, 191 bases
    to 399 bases, 400 bases +. 140->190 is perfect for 
    merging, 400+ is crazy long.
"""

def return_metrics(fn):
    short = 0
    mergeable = 0
    long = 0
    crazy_long = 0

    with open(fn, 'r') as f:
        header = True
        for line in f:
            if line.startswith('insert'):
               header = False
            else:
               if header == True:
                   continue  
               # line[0] is fragment length, line[1] is frequency
               cols = line.strip().split('\t')
               try:
                   frag, freq = map(int, cols)
               except:
                   continue  # malformed or empty line
               if frag < 130:
                   short += freq
               elif frag < 191:
                   mergeable += freq
               elif frag < 400:
                   long += freq
               else:
                   crazy_long += freq
               
    total = short + mergeable + long + crazy_long
    d = {'Fragments <130 bp':"{0:.2f}".format((short/total)*100),
         'Fragments 130-190 bp':"{0:.2f}".format((mergeable/total)*100),
         'Fragments 191-399 bp':"{0:.2f}".format((long/total)*100),
         'Fragments 400+ bp':"{0:.2f}".format((crazy_long/total)*100)}
    return d

def main():
    print(return_metrics(sys.argv[1]))


if __name__ == '__main__':
    main()
