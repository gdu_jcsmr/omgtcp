#!/bin/bash
# script to submit iqtree run to run under batch
# needs the remote .ssh file to be more flexible

topdir=${WORKDIR:-/mnt/OMG}
codedir=$HOME/omg/code

cd $topdir
s=IQTREE
cat >SUBMIT/IQTREE <<-CMD
	export WORKDIR=$topdir
	cd \$WORKDIR
	/bin/bash $codedir/run-mktree.sh ~/.ssh/nectar
	CMD

if [ -f BATCH/* ] ; then
    echo Sample $( cd BATCH; echo BATCH/*) is currently being processed.
    echo $s is scheduled to run later.
else
    echo Start processing $s
    $codedir/cronjob.sh
fi
