#!/usr/bin/env python3
"""
Created on Mon Feb  5 00:57:26 2018

@author: Bob Buckley ABC John Curtin School of Medical Research, Auystralian National University

Filter FASTQ reads.
File 1 is a list of read identifiers (use - for stdin).
File 2 is the FASTQ _R1 file
File 3 is the name of the filtered R1 FASTQ output file.
The _R2 filenames will be created automatically.
"""

import sys
import fasta

def xopen(fn):
    return sys.stdin if fn=='-' else open(fn)

def fqr2name(fn1):
    pos = fn1.rfind('_R1')
    assert pos>=0
    return fn1[:pos+2]+"2"+fn1[pos+3:]  
    
def main():
    assert len(sys.argv)==4
    idfile = sys.argv[1]
    sfn1 = sys.argv[2]
    sfn2 = fqr2name(sfn1)
    dfn1 = sys.argv[3]
    dfn2 = fqr2name(dfn1)
    # read the list of required read identifiers
    with xopen(idfile) as src:
        idset = frozenset(x.strip() for x in src)
    print("no. read IDs =", len(idset), file=sys.stderr)
    # read a SAM file and output filtered R1 and R2 FASTQ files
    mcnt, rcnt = 0, 0
    with open(dfn1, "wt") as dst1, open(dfn2, "wt") as dst2:
        src1, src2 = [fasta.fastq_reader(fn) for fn in (sfn1, sfn2)]
        for r1, r2 in zip(src1, src2): # paired reads are adjacent
            rcnt += 1
            # get ID parts of the header lines
            r1x, r2x = [r[0].split(None, 1)[0] for r in (r1, r2)]
            #print(r1x)
            #print(next(x for x in idset))
            #print('===')
            if r1x in idset:
                mcnt += 1
                assert r2x==r1x # check Read IDs match
                for r, dst in ((r1, dst1), (r2, dst2)):
                    print('@'+r[0], file=dst)
                    print(r[1], file=dst) # sequence
                    print('+', file=dst)
                    print(r[2], file=dst) # quality
    print("no. matches =", mcnt, "from", rcnt, "reads.", file=sys.stderr)
    return

if __name__=='__main__':
    main()
