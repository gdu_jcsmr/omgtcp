#!/bin/bash
set -x

# script to gather all sample qual pipeline JSON outputs and combine them into a single JSON file

[ $# -eq 0 ] && echo "Please include path to SSH key file" && exit

#sshfile=~/.ssh/nectar
sshfile=$1

WORKDIR=${WORKDIR:-/mnt/OMG}
codedir=${codedir:-$HOME/omg/code}
cd $WORKDIR
[ -d sample_qual ] && rm -r sample_qual
mkdir -p sample_qual/qualtmp
cd sample_qual/qualtmp

ln $WORKDIR/*/*.json .

# get remote sample data
# first parameter is the name of the SSH key 
[ $# -eq 1 ] && nodes="$(tail -n +2 ~/host-list | cut -f 2)"
# All other parameters are node names if passed in
[ $# -gt 1 ] && nodes="${@:2}"

# pull down sample qual JSON files
for ip in $nodes
do
   echo scp -i $sshfile $ip:$WORKDIR/\*/\*.json .
   scp -i $sshfile $ip:$WORKDIR/\*/\*.json .
done
echo done fetching remote files.

# if we accidentially copied any earlier combined reports, get rid of them
if [[ -f sample_qual_info.json ]] ; then
    rm sample_qual_info.json
fi
# Now combine all JSON files in the qualtmp subdirectory into one report
cd ..
$codedir/combine_sample_qual_json.py qualtmp sample_qual_info.json

# clean up qualtmp dir
rm -r qualtmp

# produce target plots and sample qual CSV
$codedir/sample_qual_stats.py sample_qual_info.json
$codedir/sample_qual_targets.py sample_qual_info.json
